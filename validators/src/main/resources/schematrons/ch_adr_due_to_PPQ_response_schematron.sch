<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE schema[
    <!ENTITY ch_adr_response_common_part_1_SoapHeader SYSTEM "common_parts/ch_adr_response_common_part_1_SoapHeader.txt">
    <!ENTITY ch_adr_response_common_part_2_SoapBody SYSTEM "common_parts/ch_adr_response_common_part_2_SoapBody.txt">
    <!ENTITY ch_adr_response_common_part_3_ResponseStatus SYSTEM "common_parts/ch_adr_response_common_part_3_ResponseStatus.txt">   
    <!ENTITY ch_adr_response_common_part_4_BodyAssertion SYSTEM "common_parts/ch_adr_response_common_part_4_BodyAssertion.txt">    
    <!ENTITY ch_adr_response_common_part_5_AssertionIssuer SYSTEM "common_parts/ch_adr_response_common_part_5_AssertionIssuer.txt"> 
    <!ENTITY ch_adr_response_common_part_6_AssertionStatement SYSTEM "common_parts/ch_adr_response_common_part_6_AssertionStatement.txt">
    <!ENTITY ch_adr_response_common_part_7_StatementResponse SYSTEM "common_parts/ch_adr_response_common_part_7_StatementResponse.txt">
    <!ENTITY ch_adr_response_common_part_8_ResponseResultStatus SYSTEM "common_parts/ch_adr_response_common_part_8_ResponseResultStatus.txt">
]>
<sch:schema xmlns:sch="http://purl.oclc.org/dsdl/schematron" queryBinding="xslt2"
        xmlns:sqf="http://www.schematron-quickfix.com/validator/process">
    

            <!-- Namespaces -->
            <sch:ns uri="http://www.w3.org/2001/XMLSchema-instance" prefix="xsi"/>
            <sch:ns uri="urn:e-health-suisse:2015:policy-administration" prefix="epr"/>
            <sch:ns uri="urn:oasis:names:tc:SAML:2.0:assertion" prefix="saml"/>
            <sch:ns uri="http://www.w3.org/2003/05/soap-envelope" prefix="soap"/>
            <sch:ns uri="urn:oasis:names:tc:xacml:2.0:policy:schema:os" prefix="xacml"/>
            <sch:ns uri="urn:oasis:xacml:2.0:saml:assertion:schema:os" prefix="xacml-saml" />
            <sch:ns uri="urn:oasis:names:tc:xacml:2.0:profile:saml2.0:v2:schema:protocol" prefix="xacml-samlp"/>
            <sch:ns uri="urn:oasis:names:tc:xacml:2.0:context:schema:os" prefix="xacml-context"/>
            <sch:ns uri="urn:oasis:names:tc:SAML:2.0:protocol" prefix="samlp" />
            <sch:ns uri="urn:hl7-org:v3" prefix="hl7"/>

    <!-- Rules for the general structure of an ADR due to PPQ response
        @author d.zandes -->
    <sch:pattern>
        
        <sch:let name="regex_urn_oid" value="string('^urn:oid:(\d+\.)*\d+$')" />
        <sch:let name="regex_urn" value="string('^urn:(([a-zA-Z0-9_.-])+:)*([a-zA-Z0-9])+$')" />

        <sch:let name="xds_resourceid" value="string('urn:e-health-suisse:2015:epr-subset:(\d)+:([a-zA-Z])+')"/>
        <sch:let name="resp_status_code" value="//samlp:Response/samlp:Status/samlp:StatusCode/@Value" />
        <sch:let name="indeter" value="string('Indeterminate')" />
        
        <!-- Rule 1: /soap:Header -->
        &ch_adr_response_common_part_1_SoapHeader;
        
        <!-- Rule 2: /soap:Body -->
        &ch_adr_response_common_part_2_SoapBody;
        
        <!-- Rule 2: /soap:Body/samlp:Response/samlp:Status -->
        &ch_adr_response_common_part_3_ResponseStatus;
        
        <!-- Rule 3: /soap:Body/saml:Assertion -->
        &ch_adr_response_common_part_4_BodyAssertion;
        
        <!-- Rule 4: /soap:Body/saml:Assertion/saml:Issuer -->
        &ch_adr_response_common_part_5_AssertionIssuer;
        
        <!-- Rule 6: /soap:Body/saml:Assertion/saml:Statement -->
        &ch_adr_response_common_part_6_AssertionStatement;
        
        <!-- Rule 7: /soap:Body/saml:Assertion/saml:Statement/xacml-context:Response -->
        &ch_adr_response_common_part_7_StatementResponse;
        
        <!-- Rule 8: /soap:Body/saml:Assertion/saml:Statement/xacml-context:Response/xacml-context:Result -->
        <sch:rule context="/soap:Envelope/soap:Body/samlp:Response/saml:Assertion/saml:Statement/xacml-context:Response/xacml-context:Result">
            
            <!-- CH-ADR-051 -->
            <sch:assert test="@ResourceId != ''">
                Error : The Result child element (under Statement/Response) shall contain a non-empty attribute named ResourceId
            </sch:assert>
            <sch:report test="@ResourceId != ''">
                Success : The Result child element (under Statement/Response) shall contain a non-empty attribute named ResourceId
            </sch:report>
            
            <!-- CH-ADR-050 : CH-ADR-054 -->
            <sch:assert test="count(*) = 2
                            and count(xacml-context:Decision) = 1
                            and count(xacml-context:Status) = 1">
                Error : The Result child element (under Statement/Response) shall convey 2 child elements, namely xacml-context:Decision and xacml-context:Status
            </sch:assert>
            <sch:report test="count(*) = 2
                            and count(xacml-context:Decision) = 1
                            and count(xacml-context:Status) = 1">
                Success : The Result child element (under Statement/Response) shall convey 2 child elements, namely xacml-context:Decision and xacml-context:Status
            </sch:report>
            
            <sch:assert test="*[1] = xacml-context:Decision
                            and *[2] = xacml-context:Status">
                Error : The element xacml-context:Decision (under Statement/Response/Result) must appear before element xacml-context:Status
            </sch:assert>
            <sch:report test="*[1] = xacml-context:Decision
                            and *[2] = xacml-context:Status">
                Success : The element xacml-context:Decision (under Statement/Response/Result) must appear before element xacml-context:Status
            </sch:report>
            
            <sch:assert test="xacml-context:Decision != ''">
                Error : Decision child element (under Statement/Response/Result) shall not be empty 
            </sch:assert>
            <sch:report test="xacml-context:Decision != ''">
                Success : Decision child element (under Statement/Response/Result) shall not be empty
            </sch:report>
            
            <sch:assert test="xacml-context:Decision = 'Permit'
                            or xacml-context:Decision = 'Deny'
                            or xacml-context:Decision = 'NotApplicable'
                            or xacml-context:Decision = 'Indeterminate'">
                Error : Decision child element shall hold the decision value for the equivalent Resource it applies to. Possible values are Permit, Deny, NotApplicable and Indeterminate
            </sch:assert>
            <sch:report test="xacml-context:Decision = 'Permit'
                            or xacml-context:Decision = 'Deny'
                            or xacml-context:Decision = 'NotApplicable'
                            or xacml-context:Decision = 'Indeterminate'">
                Success : Decision child element shall hold the decision value for the equivalent Resource it applies to. Possible values are Permit, Deny, NotApplicable and Indeterminate
            </sch:report>
            
        </sch:rule>
        
        <!-- Rule 9: /soap:Body/saml:Assertion/saml:Statement/xacml-context:Response/xacml-context:Result/xacml-context:Status -->
        &ch_adr_response_common_part_8_ResponseResultStatus;
        
    </sch:pattern>

</sch:schema>