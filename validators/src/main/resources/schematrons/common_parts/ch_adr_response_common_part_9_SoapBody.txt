<?xml version="1.0" encoding="UTF-8"?>

<sch:rule context="/soap:Envelope/soap:Body">

    <sch:assert test="count(*) = 1 and count(samlp:Response) = 1 or count(soap:Fault) = 1">
        Error : A samlp:Response element shall be conveyed within the message body
    </sch:assert>
    <sch:report test="count(*) = 1 and count(samlp:Response) = 1 or count(soap:Fault) = 1">
        Success : A samlp:Response element shall be conveyed within the message body
    </sch:report>

</sch:rule>

<sch:rule context="/soap:Envelope/soap:Body/samlp:Response">

    <!-- CH-ADR-045 -->
    <sch:assert test="count(*) = 2">
        Error : The samlp:Response inside the message body shall convey two (2) child elements, namely samlp:Status and saml:Assertion
    </sch:assert>
    <sch:report test="count(*) = 2">
        Success : The samlp:Response inside the message body shall convey two (2) child elements, namely samlp:Status and saml:Assertion
    </sch:report>

    <sch:assert test="count(saml:Assertion) = 1">
        Error : A SAML v2.0 Assertion shall be conveyed within the Response message body
    </sch:assert>
    <sch:report test="count(saml:Assertion) = 1">
        Success : A SAML v2.0 Assertion shall be conveyed within the Response message body
    </sch:report>

    <sch:assert test="count(samlp:Status) = 1">
        Error : A samlp:Status element shall be conveyed within the Response message body
    </sch:assert>
    <sch:report test="count(samlp:Status) = 1">
        Success : A samlp:Status element shall be conveyed within the Response message body
    </sch:report>

</sch:rule>