<?xml version="1.0" encoding="UTF-8"?>

<sch:schema xmlns:sch="http://purl.oclc.org/dsdl/schematron" queryBinding="xslt2"
            xmlns:sqf="http://www.schematron-quickfix.com/validator/process"
            xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <!-- Namespaces -->
    <sch:ns uri="http://www.w3.org/2001/XMLSchema-instance" prefix="xsi"/>
    <sch:ns uri="urn:e-health-suisse:2015:policy-administration" prefix="epr"/>
    <sch:ns uri="urn:oasis:names:tc:SAML:2.0:assertion" prefix="saml"/>
    <sch:ns uri="http://www.w3.org/2003/05/soap-envelope" prefix="soap"/>
    <sch:ns uri="urn:oasis:names:tc:xacml:2.0:policy:schema:os" prefix="xacml"/>
    <sch:ns uri="urn:oasis:names:tc:xacml:2.0:profile:saml2.0:v2:schema:protocol" prefix="xacml-samlp"/>
    <sch:ns uri="urn:oasis:names:tc:xacml:2.0:context:schema:os" prefix="xacml-context"/>
    <sch:ns uri="urn:hl7-org:v3" prefix="hl7"/>

    <!-- @author a.moustakas -->

    <!-- Variables declared for further use -->
    <sch:let name="regex_oid" value="string('^(\d+\.)*\d+$')"/>
    <sch:let name="regex_urn" value="string('^urn:oid:(\d+\.)*\d+$')"/>

    <sch:pattern>

        <!-- CH-PPQ-0040, CH-PPQ-0053, CH-PPQ-0071-->
        <sch:rule context="/">

            <sch:assert test="count(*)=1 and
                    (count(//epr:AddPolicyRequest) = 1 
                        or count(//epr:DeletePolicyRequest) = 1 
                        or count(//epr:UpdatePolicyRequest) = 1
                        or count(//xacml-samlp:XACMLPolicyQuery) = 1)">
                Error : The element soap:Body shall contain a child element named epr:AddPolicyRequest or epr:DeletePolicyRequest or
                epr:UpdatePolicyRequest or xacml-samlp:XACMLPolicyQuery
            </sch:assert>
            <sch:report test="count(*)=1 and
                    (count(/epr:AddPolicyRequest) = 1 
                        or count(/epr:DeletePolicyRequest) = 1 
                        or count(/epr:UpdatePolicyRequest) = 1
                        or count(/xacml-samlp:XACMLPolicyQuery) = 1)">
                Success : The element soap:Body shall contain a child element named epr:AddPolicyRequest or epr:DeletePolicyRequest or
                epr:UpdatePolicyRequest or xacml-samlp:XACMLPolicyQuery
            </sch:report>

        </sch:rule>

    </sch:pattern>


    <!-- Case 1: AddPolicy -->
    <sch:pattern>

        <sch:rule context="//epr:AddPolicyRequest">

            <!-- CH-PPQ-0041-->
            <sch:assert test="count(saml:Assertion) = 1">
                Error : The element epr:AddPolicyRequest shall have exactly one child element named saml:Assertion
            </sch:assert>
            <sch:report test="count(saml:Assertion) = 1">
                Success : The element epr:AddPolicyRequest shall have exactly one child element named saml:Assertion
            </sch:report>

        </sch:rule>

        <sch:rule context="//epr:AddPolicyRequest/saml:Assertion">

            <!-- CH-PPQ-0044 -->
            <sch:assert test="count(*) = 2 
                            and count(saml:Issuer) = 1
                            and count(saml:Statement) = 1">
                Error : The saml:Assertion element shall have 2 child elements, meaning exactly one saml:Issuer element (for identifying the home
                community ID
                of the Authorization Decision Provider community encoded as a URN) and exactly one saml:Statement element to convey the XACML Policy
                (-ies)
                or Policy Sets to be added
            </sch:assert>
            <sch:report test="count(*) = 2 
                            and count(saml:Issuer) = 1
                            and count(saml:Statement) = 1">
                Success : The saml:Assertion element shall have 2 child elements, meaning exactly one saml:Issuer element (for identifying the home
                community ID
                of the Authorization Decision Provider community encoded as a URN) and exactly one saml:Statement element to convey the XACML Policy
                (-ies)
                or Policy Sets to be added
            </sch:report>

        </sch:rule>

        <sch:rule context="//epr:AddPolicyRequest/saml:Assertion/saml:Issuer">

            <!-- CH-PPQ-0045 -->
            <sch:assert test="@NameQualifier='urn:e-health-suisse:community-index'">
                Error : The element saml:Issuer shall contain an attribure named NameQualifier with value equal to urn:e-health-suisse:community-index
            </sch:assert>
            <sch:report test="@NameQualifier='urn:e-health-suisse:community-index'">
                Success : The element saml:Issuer shall contain an attribure named NameQualifier with value equal to
                urn:e-health-suisse:community-index
            </sch:report>

            <sch:assert test="matches(.,$regex_urn)">
                Error : The element saml:Issuer shall contain the home community ID of the Authorization Decision Provider community encoded as an URN
            </sch:assert>
            <sch:report test="matches(.,$regex_urn)">
                Success : The element saml:Issuer shall contain the home community ID of the Authorization Decision Provider community encoded as an
                URN
            </sch:report>

        </sch:rule>

        <!-- CH-PPQ-0044 -->
        <sch:rule context="//epr:AddPolicyRequest/saml:Assertion/saml:Statement">

            <sch:assert test="count(xacml:Policy) >= 1 
                            or count(xacml:PolicySet) >=1">
                Error : The element saml:Statement shall have either one (or more) non-empty xacml:Policy child element (s) or one (or more) non-empty
                xacml:PolicySet child element (s)
                in order to convey the XACML Policy (-ies) or Policy Sets to be added
            </sch:assert>
            <sch:report test="count(xacml:Policy) >= 1 
                            or count(xacml:PolicySet) >=1">
                Success : The element saml:Statement shall have either one (or more) non-empty xacml:Policy child element (s) or one (or more)
                non-empty xacml:PolicySet child element (s)
                in order to convey the XACML Policy (-ies) or Policy Sets to be added
            </sch:report>

        </sch:rule>

    </sch:pattern>


    <!-- Case 2: UpdatePolicy -->
    <sch:pattern>

        <sch:rule context="//epr:UpdatePolicyRequest">

            <!-- CH-PPQ-0054-->
            <sch:assert test="count(saml:Assertion) = 1">
                Error : The element epr:UpdatePolicyRequest shall have exactly one child element named saml:Assertion
            </sch:assert>
            <sch:report test="count(saml:Assertion) = 1">
                Success : The element epr:UpdatePolicyRequest shall have exactly one child element named saml:Assertion
            </sch:report>

        </sch:rule>

        <sch:rule context="//epr:UpdatePolicyRequest/saml:Assertion">

            <!-- CH-PPQ-0057 -->
            <sch:assert test="count(*) = 2 
                            and count(saml:Issuer) = 1
                            and count(saml:Statement) = 1">
                Error : The saml:Assertion element shall have 2 child elements, meaning exactly one saml:Issuer element (for identifying the home
                community ID
                of the Authorization Decision Provider community encoded as a URN) and exactly one saml:Statement element to convey the XACML Policy
                (-ies)
                or Policy Sets to be updated
            </sch:assert>
            <sch:report test="count(*) = 2 
                            and count(saml:Issuer) = 1
                            and count(saml:Statement) = 1">
                Success : The saml:Assertion element shall have 2 child elements, meaning exactly one saml:Issuer element (for identifying the home
                community ID
                of the Authorization Decision Provider community encoded as a URN) and exactly one saml:Statement element to convey the XACML Policy
                (-ies)
                or Policy Sets to be updated
            </sch:report>

        </sch:rule>

        <!-- CH-PPQ-0059 -->
        <sch:rule context="//epr:UpdatePolicyRequest/saml:Assertion/saml:Issuer">

            <sch:assert test="@NameQualifier='urn:e-health-suisse:community-index'">
                Error : The element saml:Issuer shall contain an attribure named NameQualifier with value equal to urn:e-health-suisse:community-index
            </sch:assert>
            <sch:report test="@NameQualifier='urn:e-health-suisse:community-index'">
                Success : The element saml:Issuer shall contain an attribure named NameQualifier with value equal to
                urn:e-health-suisse:community-index
            </sch:report>

            <sch:assert test="matches(.,$regex_urn)">
                Error : The element saml:Issuer shall contain the home community ID of the Authorization Decision Provider community encoded as an URN
            </sch:assert>
            <sch:report test="matches(.,$regex_urn)">
                Success : The element saml:Issuer shall contain the home community ID of the Authorization Decision Provider community encoded as an
                URN
            </sch:report>

        </sch:rule>

        <!-- CH-PPQ-0055 -->
        <sch:rule context="//epr:UpdatePolicyRequest/saml:Assertion/saml:Statement">

            <sch:assert test="count(xacml:Policy) >= 1 
                            or count(xacml:PolicySet) >=1">
                Error : The element saml:Statement shall have either one (or more) non-empty xacml:Policy child element (s) or one (or more) non-empty
                xacml:PolicySet child element (s)
                in order to convey the XACML Policy (-ies) or Policy Sets to be updated
            </sch:assert>
            <sch:report test="count(xacml:Policy) >= 1 
                            or count(xacml:PolicySet) >=1">
                Success : The element saml:Statement shall have either one (or more) non-empty xacml:Policy child element (s) or one (or more)
                non-empty xacml:PolicySet child element (s)
                in order to convey the XACML Policy (-ies) or Policy Sets to be updated
            </sch:report>

        </sch:rule>

    </sch:pattern>


    <!-- Case 3: DeletePolicy -->
    <sch:pattern>

        <sch:rule context="//epr:DeletePolicyRequest">

            <!-- CH-PPQ-0072-->
            <sch:assert test="count(saml:Assertion) = 1">
                Error : The element epr:DeletePolicyRequest shall have exactly one child element named saml:Assertion
            </sch:assert>
            <sch:report test="count(saml:Assertion) = 1">
                Success : The element epr:DeletePolicyRequest shall have exactly one child element named saml:Assertion
            </sch:report>

        </sch:rule>

        <sch:rule context="//epr:DeletePolicyRequest/saml:Assertion">

            <!-- CH-PPQ-0073, CH-PPQ-0075 -->
            <sch:assert test="count(*) = 2 
                            and count(saml:Issuer) = 1
                            and count(saml:Statement) = 1">
                Error : The saml:Assertion element shall have 2 child elements, meaning exactly one saml:Issuer element (for identifying the home
                community ID
                of the Authorization Decision Provider community encoded as a URN) and exactly one saml:Statement element to convey the XACML Policy
                (-ies)
                or Policy Sets to be deleted
            </sch:assert>
            <sch:report test="count(*) = 2 
                            and count(saml:Issuer) = 1
                            and count(saml:Statement) = 1">
                Success : The saml:Assertion element shall have 2 child elements, meaning exactly one saml:Issuer element (for identifying the home
                community ID
                of the Authorization Decision Provider community encoded as a URN) and exactly one saml:Statement element to convey the XACML Policy
                (-ies)
                or Policy Sets to be deleted
            </sch:report>

        </sch:rule>

        <!-- CH-PPQ-0077 -->
        <sch:rule context="//epr:DeletePolicyRequest/saml:Assertion/saml:Issuer">

            <sch:assert test="@NameQualifier='urn:e-health-suisse:community-index'">
                Error : The element saml:Issuer shall contain an attribure named NameQualifier with value equal to urn:e-health-suisse:community-index
            </sch:assert>
            <sch:report test="@NameQualifier='urn:e-health-suisse:community-index'">
                Success : The element saml:Issuer shall contain an attribure named NameQualifier with value equal to
                urn:e-health-suisse:community-index
            </sch:report>

            <sch:assert test="matches(.,$regex_urn)">
                Error : The element saml:Issuer shall contain the home community ID of the Authorization Decision Provider community encoded as an URN
            </sch:assert>
            <sch:report test="matches(.,$regex_urn)">
                Success : The element saml:Issuer shall contain the home community ID of the Authorization Decision Provider community encoded as an
                URN
            </sch:report>

        </sch:rule>

        <!-- CH-PPQ-0073-->
        <sch:rule context="//epr:DeletePolicyRequest/saml:Assertion/saml:Statement">

            <sch:assert test="every $i in . satisfies matches($i/@xsi:type, '(.*:)?XACMLPolicySetIdReferenceStatementType')">
                Error : The Statement child element of the SAML Assertion conveyed within the DeletePolicyRequest's body shall contain a non-empty
                attribute with name equal to xsi:type
                and value equal to epd:XACMLPolicySetIdReferenceStatementType
            </sch:assert>
            <sch:report test="every $i in . satisfies matches($i/@xsi:type, '(.*:)?XACMLPolicySetIdReferenceStatementType')">
                Success : The Statement child element of the SAML Assertion conveyed within the DeletePolicyRequest's body shall contain a non-empty
                attribute with name equal to xsi:type
                and value equal to epd:XACMLPolicySetIdReferenceStatementType
            </sch:report>

            <sch:assert test="count(xacml:PolicySetIdReference) >= 1
                            and *[1] != ''">
                Error : The element saml:Statement shall have one (or more) non-empty PolicySetIdReference child element (s) to convey the XACML
                Policy (-ies) or Policy Sets to be deleted
            </sch:assert>
            <sch:report test="count(xacml:PolicySetIdReference) >= 1 
                            and *[1] != ''">
                Success : The element saml:Statement shall have one (or more) non-empty PolicySetIdReference child element (s) to convey the XACML
                Policy (-ies) or Policy Sets to be deleted
            </sch:report>

        </sch:rule>

    </sch:pattern>


    <!-- Case 4: XACMLPolicyQuery -->
    <sch:pattern>

        <sch:rule context="//xacml-samlp:XACMLPolicyQuery">

            <!-- CH-PPQ-0020-->
            <sch:assert test="count(xacml-context:Request) = 1
                              or count(xacml:PolicySetIdReference) = 1
                              or count(xacml:PolicyIdReference) = 1">
                Error : The element xacml-samlp:XACMLPolicyQuery shall have a child element named Request, PolicyIdReference or PolicySetIdReference
            </sch:assert>
            <sch:report test="count(xacml-context:Request) = 1
                              or count(xacml:PolicySetIdReference) = 1
                              or count(xacml:PolicyIdReference) = 1">
                Success : The element xacml-samlp:XACMLPolicyQuery shall have a child element named Request, PolicyIdReference or PolicySetIdReference
            </sch:report>

        </sch:rule>

        <sch:rule context="//xacml-samlp:XACMLPolicyQuery/xacml-context:Request">

            <sch:assert test="count(*) >= 4
                            and count(xacml-context:Subject) = 1
                            and count(xacml-context:Resource) >= 1
                            and count(xacml-context:Action) = 1
                            and count(xacml-context:Environment) = 1">
                Error : The Request element shall contain at least 4 child elements (one Subject, one (or more) Resource (s), one Action, one
                Environment). All of them shall comply to the namespace xmlns:xacml-context="urn:oasis:names:tc:xacml:2.0:context:schema:os" (e.g.
                xacml-context:Action)
            </sch:assert>
            <sch:report test="count(*) >= 4
                            and count(xacml-context:Subject) = 1
                            and count(xacml-context:Resource) >= 1
                            and count(xacml-context:Action) = 1
                            and count(xacml-context:Environment) = 1">
                Success : The Request element shall contain at least 4 child elements (one Subject, one (or more) Resource (s), one Action, one
                Environment). All of them shall comply to the namespace xmlns:xacml-context="urn:oasis:names:tc:xacml:2.0:context:schema:os" (e.g.
                xacml-context:Action)
            </sch:report>

            <sch:assert test="count(xacml-context:Subject) = 1">
                Error : Request element shall contain exactly one child element named Subject
            </sch:assert>
            <sch:report test="count(xacml-context:Subject) = 1">
                Success : Request element shall contain exactly one child element named Subject
            </sch:report>

            <!-- CH-PPQ-0021-->
            <sch:assert test="count(xacml-context:Resource) >= 1">
                Error : Request element shall contain one or more child elements named Resource
            </sch:assert>
            <sch:report test="count(xacml-context:Resource) >= 1">
                Success : Request element shall contain one or more child elements named Resource
            </sch:report>

            <sch:assert test="count(xacml-context:Action) = 1">
                Error : Request element shall contain exactly one child element named Action
            </sch:assert>
            <sch:report test="count(xacml-context:Action) = 1">
                Success : Request element shall contain exactly one child element named Action
            </sch:report>

            <sch:assert test="count(xacml-context:Environment) = 1">
                Error : Request element shall contain exactly one child element named Environment
            </sch:assert>
            <sch:report test="count(xacml-context:Environment) = 1">
                Success : Request element shall contain exactly one child element named Environment
            </sch:report>

        </sch:rule>

        <!-- Request:Subject -->
        <sch:rule context="//xacml-samlp:XACMLPolicyQuery/xacml-context:Request/xacml-context:Subject">

            <sch:assert test="count(*) = 0">
                Error : The Subject element conveyed under Request shall be empty as it has no PPQ usecase yet
            </sch:assert>
            <sch:report test="count(*) = 0">
                Success : The Subject element conveyed under Request shall be empty as it has no PPQ usecase yet
            </sch:report>

        </sch:rule>

        <!-- Request:Resource -->
        <sch:rule context="//xacml-samlp:XACMLPolicyQuery/xacml-context:Request/xacml-context:Resource">

            <sch:assert test="count(xacml-context:Attribute) = 1">
                Error : The Resource element shall contain exactly one Attribute child element that shall comply to the namespace
                xmlns:xacml-context="urn:oasis:names:tc:xacml:2.0:context:schema:os"
                (e.g. xacml-context:Attribute)
            </sch:assert>
            <sch:report test="count(xacml-context:Attribute) = 1">
                Success : The Resource element shall contain exactly one Attribute child element that shall comply to the namespace
                xmlns:xacml-context="urn:oasis:names:tc:xacml:2.0:context:schema:os"
                (e.g. xacml-context:Attribute)
            </sch:report>

            <sch:assert test="count(*[@AttributeId='urn:e-health-suisse:2015:epr-spid']) = 1">
                Error : The Resource element shall contain exactly one Attribute child element with AttributeId equal to
                urn:e-health-suisse:2015:epr-spid
            </sch:assert>
            <sch:report test="count(*[@AttributeId='urn:e-health-suisse:2015:epr-spid']) = 1">
                Success : The Resource element shall contain exactly one Attribute child element with AttributeId equal to
                urn:e-health-suisse:2015:epr-spid
            </sch:report>

            <sch:assert test="every $i in *[@AttributeId='urn:e-health-suisse:2015:epr-spid'] satisfies matches($i/@DataType,'(.*:)?II')">
                Error : The DataType of Resource's Attribute child element with AttributeId equal to urn:e-health-suisse:2015:epr-spid shall be
                considered a II (InstanceIdentifier) and - as a result - set to urn:hl7-org:v3#II
            </sch:assert>
            <sch:report test="every $i in *[@AttributeId='urn:e-health-suisse:2015:epr-spid'] satisfies matches($i/@DataType,'(.*:)?II')">
                Success : The DataType of Resource's Attribute child element with AttributeId equal to urn:e-health-suisse:2015:epr-spid shall be
                considered a II (InstanceIdentifier) and - as a result - set to urn:hl7-org:v3#II
            </sch:report>

            <!-- Check if Attribute/AttributeValues are empty -->
            <sch:assert test="count(*[@AttributeId='urn:e-health-suisse:2015:epr-spid']/xacml-context:AttributeValue) = 1">
                Error : The Resource's Attribute child element with AttributeId equal to urn:e-health-suisse:2015:epr-spid shall convey a non-empty
                AttributeValue element that shall comply to the
                namespace xmlns:xacml-context="urn:oasis:names:tc:xacml:2.0:context:schema:os" (e.g. xacml-context:AttributeValue)
            </sch:assert>
            <sch:report test="count(*[@AttributeId='urn:e-health-suisse:2015:epr-spid']/xacml-context:AttributeValue) = 1">
                Success : The Resource's Attribute child element with AttributeId equal to urn:e-health-suisse:2015:epr-spid shall convey a non-empty
                AttributeValue element that shall comply to the
                namespace xmlns:xacml-context="urn:oasis:names:tc:xacml:2.0:context:schema:os" (e.g. xacml-context:AttributeValue)
            </sch:report>

            <!-- Check if Attribute/AttributeValues follow their data type -->
            <sch:assert test="*[@AttributeId='urn:e-health-suisse:2015:epr-spid']/xacml-context:AttributeValue/hl7:InstanceIdentifier">
                Error : The AttributeValue for Resource's Attribute child element with AttributeId equal to urn:e-health-suisse:2015:epr-spid shall
                convey the patient's national identifier as an hl7:InstanceIdentifier element
            </sch:assert>
            <sch:report test="*[@AttributeId='urn:e-health-suisse:2015:epr-spid']/xacml-context:AttributeValue/hl7:InstanceIdentifier">
                Success : The AttributeValue for Resource's Attribute child element with AttributeId equal to urn:e-health-suisse:2015:epr-spid shall
                convey the patient's national identifier as an hl7:InstanceIdentifier element
            </sch:report>

            <sch:assert test="*[@AttributeId='urn:e-health-suisse:2015:epr-spid']/xacml-context:AttributeValue/hl7:InstanceIdentifier/@root != ''">
                Error : The hl7:CodedValue element conveyed within the AttributeValue of the Resource's Attribute child element with AttributeId equal
                to urn:e-health-suisse:2015:epr-spid shall contain a
                non-empty root attribute
            </sch:assert>
            <sch:report test="*[@AttributeId='urn:e-health-suisse:2015:epr-spid']/xacml-context:AttributeValue/hl7:InstanceIdentifier/@root != ''">
                Success : The hl7:CodedValue element conveyed within the AttributeValue of the Resource's Attribute child element with AttributeId
                equal to urn:e-health-suisse:2015:epr-spid shall contain a
                non-empty root attribute
            </sch:report>

            <sch:assert
                    test="matches(*[@AttributeId='urn:e-health-suisse:2015:epr-spid']/xacml-context:AttributeValue/hl7:InstanceIdentifier/@root, $regex_oid)">
                Error : The hl7:InstanceIdentifier element conveyed within the AttributeValue of the Resource's Attribute child element with
                AttributeId equal to urn:e-health-suisse:2015:epr-spid shall contain a
                non-empty root attribute that follows OID syntax
            </sch:assert>
            <sch:report
                    test="matches(*[@AttributeId='urn:e-health-suisse:2015:epr-spid']/xacml-context:AttributeValue/hl7:InstanceIdentifier/@root, $regex_oid)">
                Success : The hl7:InstanceIdentifier element conveyed within the AttributeValue of the Resource's Attribute child element with
                AttributeId equal to urn:e-health-suisse:2015:epr-spid shall contain a
                non-empty root attribute that follows OID syntax
            </sch:report>

            <sch:assert
                    test="*[@AttributeId='urn:e-health-suisse:2015:epr-spid']/xacml-context:AttributeValue/hl7:InstanceIdentifier/@extension != ''">
                Error : The hl7:CodedValue element conveyed within the AttributeValue of the Resource's Attribute child element with AttributeId equal
                to urn:e-health-suisse:2015:epr-spid shall contain a
                non-empty extension attribute
            </sch:assert>
            <sch:report
                    test="*[@AttributeId='urn:e-health-suisse:2015:epr-spid']/xacml-context:AttributeValue/hl7:InstanceIdentifier/@extension != ''">
                Success : The hl7:CodedValue element conveyed within the AttributeValue of the Resource's Attribute child element with AttributeId
                equal to urn:e-health-suisse:2015:epr-spid shall contain a
                non-empty extension attribute
            </sch:report>

        </sch:rule>

        <!-- Request:Action -->
        <sch:rule context="//xacml-samlp:XACMLPolicyQuery/xacml-context:Request/xacml-context:Action">

            <sch:assert test="count(*) = 0">
                Error : The Action element conveyed under Request shall be empty as it has no PPQ usecase yet
            </sch:assert>
            <sch:report test="count(*) = 0">
                Success : The Action element conveyed under Request shall be empty as it has no PPQ usecase yet
            </sch:report>

        </sch:rule>

        <!-- Request:Environment -->
        <sch:rule context="//xacml-samlp:XACMLPolicyQuery/xacml-context:Request/xacml-context:Environment">

            <sch:assert test="count(*)=0">
                Error : The Environment element conveyed under Request shall be empty as it has no PPQ usecase yet
            </sch:assert>
            <sch:report test="count(*)=0">
                Success : The Environment element conveyed under Request shall be empty as it has no PPQ usecase yet
            </sch:report>

        </sch:rule>

    </sch:pattern>

</sch:schema>
