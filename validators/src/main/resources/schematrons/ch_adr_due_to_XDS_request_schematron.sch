<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE schema[
    <!ENTITY ch_adr_common_part_1_XACMLAuthzDecisionQuery SYSTEM "common_parts/ch_adr_common_part_1_XACMLAuthzDecisionQuery.txt">
    <!ENTITY ch_adr_common_part_2_XACMLAuthzDecisionQuery_Request SYSTEM "common_parts/ch_adr_common_part_2_XACMLAuthzDecisionQuery_Request.txt">
    <!ENTITY ch_adr_common_part_3_XACMLAuthzDecisionQuery_Request_Subject SYSTEM "common_parts/ch_adr_common_part_3_XACMLAuthzDecisionQuery_Request_Subject.txt">
    <!ENTITY ch_adr_common_part_4_XACMLAuthzDecisionQuery_Request_Action SYSTEM "common_parts/ch_adr_common_part_4_XACMLAuthzDecisionQuery_Request_Action.txt">
    <!ENTITY ch_adr_common_part_5_XACMLAuthzDecisionQuery_Request_Environment SYSTEM "common_parts/ch_adr_common_part_5_XACMLAuthzDecisionQuery_Request_Environment.txt">
]>
<sch:schema xmlns:sch="http://purl.oclc.org/dsdl/schematron" queryBinding="xslt2"
            xmlns:sqf="http://www.schematron-quickfix.com/validator/process"
            xmlns:xsl="http://www.w3.org/1999/XSL/Transform">">


            <!-- Namespaces -->
            <sch:ns uri="http://www.w3.org/2001/XMLSchema-instance" prefix="xsi"/>
            <sch:ns uri="urn:e-health-suisse:2015:policy-administration" prefix="epr"/>
            <sch:ns uri="urn:oasis:names:tc:SAML:2.0:assertion" prefix="saml"/>
            <sch:ns uri="http://www.w3.org/2003/05/soap-envelope" prefix="soap"/>
            <sch:ns uri="urn:oasis:names:tc:xacml:2.0:policy:schema:os" prefix="xacml"/>
            <sch:ns uri="urn:oasis:xacml:2.0:saml:assertion:schema:os" prefix="xacml-saml" />
            <sch:ns uri="urn:oasis:names:tc:xacml:2.0:profile:saml2.0:v2:schema:protocol" prefix="xacml-samlp"/>
            <sch:ns uri="urn:oasis:names:tc:xacml:2.0:context:schema:os" prefix="xacml-context"/>
            <sch:ns uri="urn:oasis:names:tc:SAML:2.0:protocol" prefix="samlp" />
            <sch:ns uri="urn:hl7-org:v3" prefix="hl7"/>



     <!-- Rules for the general structure of an ADR due to XDS request
         @author d.zandes -->
    <sch:pattern>

        <!-- Variables declared for further use -->
        <sch:let name="regex_oid" value="string('^(\d+\.)*\d+$')" />
        <sch:let name="regex_urn_oid" value="string('^urn:oid:(\d+\.)*\d+$')" />
        <sch:let name="regex_purpOfUse_oid" value="string('^(\d+\.)*(\d|[x]*)+$')" />
        <sch:let name="regex_urn" value="string('^urn:(([a-zA-Z0-9_.-])+:)*([a-zA-Z0-9])+$')" />

        <sch:let name="doc_class_normal" value="string('urn:e-health-suisse:2015:epr-subset:(\d)+:normal')"/>
        <sch:let name="doc_class_restricted" value="string('urn:e-health-suisse:2015:epr-subset:(\d)+:restricted')"/>
        <sch:let name="doc_class_secret" value="string('urn:e-health-suisse:2015:epr-subset:(\d)+:secret')"/>
        
        <sch:let name="role_patient" value="string('^Patient\(in\)+$')" />
        <sch:let name="role_behande" value="string('^Behandelnde\(r\)+$')" />
        <sch:let name="role_hilfspe" value="string('^Hilfsperson+$')" />
        <sch:let name="role_stellve" value="string('^Stellvertreter\(in\)+$')" />
        
        <!-- Rule 1: XACMLAuthzDecisionQuery -->
        &ch_adr_common_part_1_XACMLAuthzDecisionQuery;
            
        <!-- Rule 2: XACMLAuthzDecisionQuery/Request -->
        <sch:rule context="/xacml-samlp:XACMLAuthzDecisionQuery/xacml-context:Request">
            
            &ch_adr_common_part_2_XACMLAuthzDecisionQuery_Request;
            
            <!-- Extra rule specifically for the "ADR due to XDS" case -->
            <sch:assert test="count(xacml-context:Resource) = 3 
                            and count(matches(*[xacml-context:Attribute[@AttributeId='urn:oasis:names:tc:xacml:1.0:resource:resource-id']]/xacml-context:AttributeValue, $doc_class_normal)) = 1
                            and count(matches(*[xacml-context:Attribute[@AttributeId='urn:oasis:names:tc:xacml:1.0:resource:resource-id']]/xacml-context:AttributeValue, $doc_class_restricted)) = 1
                            and count(matches(*[xacml-context:Attribute[@AttributeId='urn:oasis:names:tc:xacml:1.0:resource:resource-id']]/xacml-context:AttributeValue, $doc_class_secret)) = 1">
                Error : For ADR due to XDS there shall always be exactly three (3) Resources to be identified, each representing a class of documents: normal, restricted and secret documents. Therefore, each of those three (3)
                Resources shall use only one of the three (3) classes (e.g. urn:e-health-suisse:2015:epr-subset:8901:normal,
                urn:e-health-suisse:2015:epr-subset:8901:restricted and urn:e-health-suisse:2015:epr-subset:8901:secret)
                    as AttributeValue for their Attribute with id equal to urn:oasis:names:tc:xacml:1.0:resource:resource-id.
            </sch:assert>
            <sch:report test="count(xacml-context:Resource) = 3 
                            and count(matches(*[xacml-context:Attribute[@AttributeId='urn:oasis:names:tc:xacml:1.0:resource:resource-id']]/xacml-context:AttributeValue, $doc_class_normal)) = 1
                            and count(matches(*[xacml-context:Attribute[@AttributeId='urn:oasis:names:tc:xacml:1.0:resource:resource-id']]/xacml-context:AttributeValue, $doc_class_restricted)) = 1
                            and count(matches(*[xacml-context:Attribute[@AttributeId='urn:oasis:names:tc:xacml:1.0:resource:resource-id']]/xacml-context:AttributeValue, $doc_class_secret)) = 1">
                Success : For ADR due to XDS there shall always be exactly three (3) Resources to be identified, each representing a class of documents: normal, restricted and secret documents. Therefore, each of those three (3)
                Resources shall use only one of the three (3) classes (e.g. urn:e-health-suisse:2015:epr-subset:8901:normal,
                urn:e-health-suisse:2015:epr-subset:8901:restricted and urn:e-health-suisse:2015:epr-subset:8901:secret)
                    as AttributeValue for their Attribute with id equal to urn:oasis:names:tc:xacml:1.0:resource:resource-id.
            </sch:report>
            
        </sch:rule>
        
        <!-- Rule 3: XACMLAuthzDecisionQuery/Request/Subject -->
        &ch_adr_common_part_3_XACMLAuthzDecisionQuery_Request_Subject;
        
        <!-- Rule 4: XACMLAuthzDecisionQuery/Request/Resource -->
        <sch:rule context="/xacml-samlp:XACMLAuthzDecisionQuery/xacml-context:Request/xacml-context:Resource">
            
            <!-- CH-ADR-29 -->
            <sch:assert test="count(xacml-context:Attribute) >= 3">
                Error: The Resource element shall contain at least 3 Attribute child elements
            </sch:assert>   
            <sch:report test="count(xacml-context:Attribute) >= 3">
                Success: The Resource element shall contain at least 3 Attribute child elements
            </sch:report> 
              
            <sch:assert test="count(*[@AttributeId='urn:oasis:names:tc:xacml:1.0:resource:resource-id']) = 1">
                Error: The Resource element shall contain an Attribute child element with AttributeId equal to urn:oasis:names:tc:xacml:1.0:resource:resource-id
            </sch:assert>
            <sch:report test="count(*[@AttributeId='urn:oasis:names:tc:xacml:1.0:resource:resource-id']) = 1">
                Success: The Resource element shall contain an Attribute child element with AttributeId equal to urn:oasis:names:tc:xacml:1.0:resource:resource-id
            </sch:report>
            
            <sch:assert test="count(*[@AttributeId='urn:e-health-suisse:2015:epr-spid']) = 1">
                Error: The Resource element shall contain an Attribute child element with AttributeId equal to urn:e-health-suisse:2015:epr-spid
            </sch:assert>
            <sch:report test="count(*[@AttributeId='urn:e-health-suisse:2015:epr-spid']) = 1">
                Success: The Resource element shall contain an Attribute child element with AttributeId equal to urn:e-health-suisse:2015:epr-spid
            </sch:report>
            
            <sch:assert test="count(*[@AttributeId='urn:ihe:iti:xds-b:2007:confidentiality-code']) = 1">
                Error: The Resource element shall contain an Attribute child element with AttributeId equal to urn:ihe:iti:xds-b:2007:confidentiality-code (when ADR due to XDS)
                        or urn:e-health-suisse:2015:policy-attributes:referenced-policy-set (when ADR due to PPQ)
            </sch:assert>
            <sch:report test="count(*[@AttributeId='urn:ihe:iti:xds-b:2007:confidentiality-code']) = 1">
                Success: The Resource element shall contain an Attribute child element with AttributeId equal to urn:ihe:iti:xds-b:2007:confidentiality-code (when ADR due to XDS)
                        or urn:e-health-suisse:2015:policy-attributes:referenced-policy-set (when ADR due to PPQ)
            </sch:report>
            
            <!-- DataType check -->
            <sch:assert test="every $i in *[@AttributeId='urn:oasis:names:tc:xacml:1.0:resource:resource-id'] satisfies matches($i/@DataType, '(.*:)?anyURI')">
                Error : The DataType of Resource's Attribute child element with AttributeId equal to urn:oasis:names:tc:xacml:1.0:resource:resource-id shall be considered a URI and - as a result - set to http://www.w3.org/2001/XMLSchema#anyURI
            </sch:assert>
            <sch:report test="every $i in *[@AttributeId='urn:oasis:names:tc:xacml:1.0:resource:resource-id'] satisfies matches($i/@DataType, '(.*:)?anyURI')">
                Success : The DataType of Resource's Attribute child element with AttributeId equal to urn:oasis:names:tc:xacml:1.0:resource:resource-id shall be considered a URI and - as a result - set to http://www.w3.org/2001/XMLSchema#anyURI
            </sch:report>
            
            <sch:assert test="every $i in *[@AttributeId='urn:e-health-suisse:2015:epr-spid'] satisfies matches($i/@DataType, '(.*:)?II')">
                Error: The DataType of Resource's Attribute child element with AttributeId equal to urn:e-health-suisse:2015:epr-spid shall be considered a II (InstanceIdentifier) and - as a result - set to urn:hl7-org:v3#II
            </sch:assert>
            <sch:report test="every $i in *[@AttributeId='urn:e-health-suisse:2015:epr-spid'] satisfies matches($i/@DataType, '(.*:)?II')">
                Success: The DataType of Resource's Attribute child element with AttributeId equal to urn:e-health-suisse:2015:epr-spid shall be considered a II (InstanceIdentifier) and - as a result - set to urn:hl7-org:v3#II
            </sch:report>
            
            <sch:assert test="every $i in *[@AttributeId='urn:ihe:iti:xds-b:2007:confidentiality-code'] satisfies matches($i/@DataType, '(.*:)?CV')">
                Error: The DataType of Resource's Attribute child element with AttributeId equal to urn:ihe:iti:xds-b:2007:confidentiality-code (when ADR due to XDS) or
                        urn:e-health-suisse:2015:policy-attributes:referenced-policy-set (when ADR due to PPQ) shall be considered a CV (CodedValue) and - as a result - set to urn:hl7-org:v3#CV
            </sch:assert>
            <sch:report test="every $i in *[@AttributeId='urn:ihe:iti:xds-b:2007:confidentiality-code'] satisfies matches($i/@DataType, '(.*:)?CV')">
                Success: The DataType of Resource's Attribute child element with AttributeId equal to urn:ihe:iti:xds-b:2007:confidentiality-code (when ADR due to XDS) or
                        urn:e-health-suisse:2015:policy-attributes:referenced-policy-set (when ADR due to PPQ) shall be considered a CV (CodedValue) and - as a result - set to urn:hl7-org:v3#CV
            </sch:report>
            
            <!-- Check if Attribute/AttributeValues are empty -->
            <sch:assert test="*[@AttributeId='urn:oasis:names:tc:xacml:1.0:resource:resource-id']/normalize-space(*)">
                Error: The AttributeValue for Resource's Attribute child element with AttributeId equal to urn:oasis:names:tc:xacml:1.0:resource:resource-id shall shall not be empty 
            </sch:assert>
            <sch:report test="*[@AttributeId='urn:oasis:names:tc:xacml:1.0:resource:resource-id']/normalize-space(*)">
                Success: The AttributeValue for Resource's Attribute child element with AttributeId equal to urn:oasis:names:tc:xacml:1.0:resource:resource-id shall shall not be empty 
            </sch:report>
            
            <!-- Check if Attribute/AttributeValues follow their data type -->
            <sch:assert test="matches(*[@AttributeId='urn:oasis:names:tc:xacml:1.0:resource:resource-id']/xacml-context:AttributeValue, $regex_urn)">
                Error: The AttributeValue for Resource's Attribute child element with AttributeId equal to urn:oasis:names:tc:xacml:1.0:resource:resource-id shall convey the resource identifier. 
                    The value's syntax shall be a URN (ADR due to XDS)
            </sch:assert>
            <sch:report test="matches(*[@AttributeId='urn:oasis:names:tc:xacml:1.0:resource:resource-id']/xacml-context:AttributeValue, $regex_urn)">
                Success: The AttributeValue for Resource's Attribute child element with AttributeId equal to urn:oasis:names:tc:xacml:1.0:resource:resource-id shall convey the resource identifier. 
                    The value's syntax shall be a URN (ADR due to XDS)
            </sch:report>
            
            <sch:assert test="matches(*[@AttributeId='urn:oasis:names:tc:xacml:1.0:resource:resource-id']/xacml-context:AttributeValue, $doc_class_normal) 
                            or matches(*[@AttributeId='urn:oasis:names:tc:xacml:1.0:resource:resource-id']/xacml-context:AttributeValue, $doc_class_restricted) 
                            or matches(*[@AttributeId='urn:oasis:names:tc:xacml:1.0:resource:resource-id']/xacml-context:AttributeValue, $doc_class_secret)">
                Error: The AttributeValue for Resource's Attribute child element with AttributeId equal to urn:oasis:names:tc:xacml:1.0:resource:resource-id shall convey the resource identifier.
                The value shall be equal to one of the three classes of documents (i.e. urn:e-health-suisse:2015:epr-subset:&#60;patient_id&#62;:normal,
                urn:e-health-suisse:2015:epr-subset:&#60;patient_id&#62;:restricted,
                urn:e-health-suisse:2015:epr-subset:&#60;patient_id&#62;:secret)
            </sch:assert>
            <sch:report test="matches(*[@AttributeId='urn:oasis:names:tc:xacml:1.0:resource:resource-id']/xacml-context:AttributeValue, $doc_class_normal) 
                            or matches(*[@AttributeId='urn:oasis:names:tc:xacml:1.0:resource:resource-id']/xacml-context:AttributeValue, $doc_class_restricted) 
                            or matches(*[@AttributeId='urn:oasis:names:tc:xacml:1.0:resource:resource-id']/xacml-context:AttributeValue, $doc_class_secret)">
                Success: The AttributeValue for Resource's Attribute child element with AttributeId equal to urn:oasis:names:tc:xacml:1.0:resource:resource-id shall convey the resource identifier.
                The value shall be equal to one of the three classes of documents (i.e. urn:e-health-suisse:2015:epr-subset:&#60;patient_id&#62;:normal,
                urn:e-health-suisse:2015:epr-subset:&#60;patient_id&#62;:restricted,
                urn:e-health-suisse:2015:epr-subset:&#60;patient_id&#62;:secret)
            </sch:report>
            
            <sch:assert test="*[@AttributeId='urn:e-health-suisse:2015:epr-spid']/xacml-context:AttributeValue/hl7:InstanceIdentifier">
                Error: The AttributeValue for Resource's Attribute child element with AttributeId equal to urn:e-health-suisse:2015:epr-spid shall convey the patient's national identifier as an hl7:InstanceIdentifier element
            </sch:assert>
            <sch:report test="*[@AttributeId='urn:e-health-suisse:2015:epr-spid']/xacml-context:AttributeValue/hl7:InstanceIdentifier">
                Success: The AttributeValue for Resource's Attribute child element with AttributeId equal to urn:e-health-suisse:2015:epr-spid shall convey the patient's national identifier as an hl7:InstanceIdentifier element
            </sch:report>
            
            <sch:assert test="*[@AttributeId='urn:e-health-suisse:2015:epr-spid']/xacml-context:AttributeValue/hl7:InstanceIdentifier/@root != ''">
                Error: The hl7:InstanceIdentifier element conveyed within the AttributeValue of the Resource's Attribute child element with AttributeId equal to urn:e-health-suisse:2015:epr-spid shall contain a 
                    non-empty root attribute
            </sch:assert>
            <sch:report test="*[@AttributeId='urn:e-health-suisse:2015:epr-spid']/xacml-context:AttributeValue/hl7:InstanceIdentifier/@root != ''">
                Success: The hl7:InstanceIdentifier element conveyed within the AttributeValue of the Resource's Attribute child element with AttributeId equal to urn:e-health-suisse:2015:epr-spid shall contain a 
                    non-empty root attribute
            </sch:report>
            
            <sch:assert test="matches(*[@AttributeId='urn:e-health-suisse:2015:epr-spid']/xacml-context:AttributeValue/hl7:InstanceIdentifier/@root, $regex_oid)">
                Error: The hl7:InstanceIdentifier element conveyed within the AttributeValue of the Resource's Attribute child element with AttributeId equal to urn:e-health-suisse:2015:epr-spid shall contain a 
                    non-empty root attribute that follows OID syntax
            </sch:assert>
            <sch:report test="matches(*[@AttributeId='urn:e-health-suisse:2015:epr-spid']/xacml-context:AttributeValue/hl7:InstanceIdentifier/@root, $regex_oid)">
                Success: The hl7:InstanceIdentifier element conveyed within the AttributeValue of the Resource's Attribute child element with AttributeId equal to urn:e-health-suisse:2015:epr-spid shall contain a 
                    non-empty root attribute that follows OID syntax
            </sch:report>
            
            <sch:assert test="*[@AttributeId='urn:e-health-suisse:2015:epr-spid']/xacml-context:AttributeValue/hl7:InstanceIdentifier/@extension != ''">
                Error: The hl7:InstanceIdentifier element conveyed within the AttributeValue of the Resource's Attribute child element with AttributeId equal to urn:e-health-suisse:2015:epr-spid shall contain a 
                    non-empty extension attribute
            </sch:assert>
            <sch:report test="*[@AttributeId='urn:e-health-suisse:2015:epr-spid']/xacml-context:AttributeValue/hl7:InstanceIdentifier/@extension != ''">
                Success: The hl7:InstanceIdentifier element conveyed within the AttributeValue of the Resource's Attribute child element with AttributeId equal to urn:e-health-suisse:2015:epr-spid shall contain a 
                    non-empty extension attribute
            </sch:report>
            
            <sch:assert test="*[@AttributeId='urn:ihe:iti:xds-b:2007:confidentiality-code']/xacml-context:AttributeValue/hl7:CodedValue">
                Error: The AttributeValue for Resource's Attribute child element with AttributeId equal to urn:ihe:iti:xds-b:2007:confidentiality-code shall convey the subject's role as an hl7:CodedValue element
            </sch:assert>
            <sch:report test="*[@AttributeId='urn:ihe:iti:xds-b:2007:confidentiality-code']/xacml-context:AttributeValue/hl7:CodedValue">
                Success: The AttributeValue for Resource's Attribute child element with AttributeId equal to urn:ihe:iti:xds-b:2007:confidentiality-code shall convey the subject's role as an hl7:CodedValue element
            </sch:report>
            
            <sch:assert test="*[@AttributeId='urn:ihe:iti:xds-b:2007:confidentiality-code']/xacml-context:AttributeValue/hl7:CodedValue/@code != ''">
                Error: The hl7:CodedValue element conveyed within the AttributeValue of the Resource's Attribute child element with AttributeId equal to urn:ihe:iti:xds-b:2007:confidentiality-code shall contain a 
                    non-empty code attribute
            </sch:assert>
            <sch:report test="*[@AttributeId='urn:ihe:iti:xds-b:2007:confidentiality-code']/xacml-context:AttributeValue/hl7:CodedValue/@code != ''">
                Success: The hl7:CodedValue element conveyed within the AttributeValue of the Resource's Attribute child element with AttributeId equal to urn:ihe:iti:xds-b:2007:confidentiality-code shall contain a 
                    non-empty code attribute
            </sch:report>


            <sch:assert test="/xacml-samlp:XACMLAuthzDecisionQuery/xacml-context:Request/xacml-context:Resource/xacml-context:Attribute/xacml-context:AttributeValue/hl7:CodedValue[@displayName='normal']/@code = '17621005'">
                 Error: The code element conveyed within the AttributeValue of the Resource's Attribute child element with displayName equal to normal shall contain the code 17621005
             </sch:assert>
            <sch:report test="/xacml-samlp:XACMLAuthzDecisionQuery/xacml-context:Request/xacml-context:Resource/xacml-context:Attribute/xacml-context:AttributeValue/hl7:CodedValue[@displayName='normal']/@code = '17621005'">
                Success: The code element conveyed within the AttributeValue of the Resource's Attribute child element with displayName equal to normal shall contain the code 17621005
            </sch:report>

            <sch:assert test="/xacml-samlp:XACMLAuthzDecisionQuery/xacml-context:Request/xacml-context:Resource/xacml-context:Attribute/xacml-context:AttributeValue/hl7:CodedValue[@displayName='restricted']/@code = '263856008'">
                Error: The code element conveyed within the AttributeValue of the Resource's Attribute child element with displayName equal to restricted shall contain the code 263856008
            </sch:assert>
            <sch:report test="/xacml-samlp:XACMLAuthzDecisionQuery/xacml-context:Request/xacml-context:Resource/xacml-context:Attribute/xacml-context:AttributeValue/hl7:CodedValue[@displayName='restricted']/@code = '263856008'">
                Success: The code element conveyed within the AttributeValue of the Resource's Attribute child element with displayName equal to restricted shall contain the code 263856008
            </sch:report>

            <sch:assert test="/xacml-samlp:XACMLAuthzDecisionQuery/xacml-context:Request/xacml-context:Resource/xacml-context:Attribute/xacml-context:AttributeValue/hl7:CodedValue[@displayName='secret']/@code = '1141000195107'">
                Error: The code element conveyed within the AttributeValue of the Resource's Attribute child element with displayName equal to secret shall contain the code 1141000195107
            </sch:assert>
            <sch:report test="/xacml-samlp:XACMLAuthzDecisionQuery/xacml-context:Request/xacml-context:Resource/xacml-context:Attribute/xacml-context:AttributeValue/hl7:CodedValue[@displayName='secret']/@code = '1141000195107'">
                Success: The code element conveyed within the AttributeValue of the Resource's Attribute child element with displayName equal to secret shall contain the code 1141000195107
            </sch:report>




            
            <sch:assert test="*[@AttributeId='urn:ihe:iti:xds-b:2007:confidentiality-code']/xacml-context:AttributeValue/hl7:CodedValue/@codeSystem != ''">
                Error: The hl7:CodedValue element conveyed within the AttributeValue of the Resource's Attribute child element with AttributeId equal to urn:ihe:iti:xds-b:2007:confidentiality-code shall contain a 
                    non-empty codeSystem attribute
            </sch:assert>
            <sch:report test="*[@AttributeId='urn:ihe:iti:xds-b:2007:confidentiality-code']/xacml-context:AttributeValue/hl7:CodedValue/@codeSystem != ''">
                Success: The hl7:CodedValue element conveyed within the AttributeValue of the Resource's Attribute child element with AttributeId equal to urn:ihe:iti:xds-b:2007:confidentiality-code shall contain a 
                    non-empty codeSystem attribute
            </sch:report>
            
            <!-- Changed regex pattern from $regex_purpOfUse_oid to $regex_oid to meet the modified Requirements -->
            <sch:assert test="matches(*[@AttributeId='urn:ihe:iti:xds-b:2007:confidentiality-code']/xacml-context:AttributeValue/hl7:CodedValue/@codeSystem, $regex_oid)">
                Error: The hl7:CodedValue element conveyed within the AttributeValue of the Resource's Attribute child element with AttributeId equal to urn:ihe:iti:xds-b:2007:confidentiality-code shall contain a 
                    non-empty codeSystem attribute that follows OID syntax
            </sch:assert>
            <sch:report test="matches(*[@AttributeId='urn:ihe:iti:xds-b:2007:confidentiality-code']/xacml-context:AttributeValue/hl7:CodedValue/@codeSystem, $regex_oid)">
                Success: The hl7:CodedValue element conveyed within the AttributeValue of the Resource's Attribute child element with AttributeId equal to urn:ihe:iti:xds-b:2007:confidentiality-code shall contain a 
                    non-empty codeSystem attribute that follows OID syntax
            </sch:report>
            
            <sch:assert test="*[@AttributeId='urn:ihe:iti:xds-b:2007:confidentiality-code']/xacml-context:AttributeValue/hl7:CodedValue/@displayName != ''">
                Error: The hl7:CodedValue element conveyed within the AttributeValue of the Resource's Attribute child element with AttributeId equal to urn:ihe:iti:xds-b:2007:confidentiality-code shall contain a 
                    non-empty displayName attribute
            </sch:assert>
            <sch:report test="*[@AttributeId='urn:ihe:iti:xds-b:2007:confidentiality-code']/xacml-context:AttributeValue/hl7:CodedValue/@displayName != ''">
                Success: The hl7:CodedValue element conveyed within the AttributeValue of the Resource's Attribute child element with AttributeId equal to urn:ihe:iti:xds-b:2007:confidentiality-code shall contain a 
                    non-empty displayName attribute
            </sch:report>
                  
        </sch:rule>
        
        <!-- Rule 5: XACMLAuthzDecisionQuery/Request/Action -->
        &ch_adr_common_part_4_XACMLAuthzDecisionQuery_Request_Action;
        
        <!-- Rule 6: XACMLAuthzDecisionQuery/Request/Action/Attribute -->
        <sch:rule context="/xacml-samlp:XACMLAuthzDecisionQuery/xacml-context:Request/xacml-context:Action/xacml-context:Attribute">
            
            <!-- CH-ADR-35 -->
            <sch:assert test="count(*) = 1 and count(/xacml-samlp:XACMLAuthzDecisionQuery/xacml-context:Request/xacml-context:Action/xacml-context:Attribute/xacml-context:AttributeValue) = 1">
                Error: The Action's Attribute child element shall contain only one child element, namely AttributeValue
            </sch:assert>
            <sch:report test="count(*) = 1 and count(/xacml-samlp:XACMLAuthzDecisionQuery/xacml-context:Request/xacml-context:Action/xacml-context:Attribute/xacml-context:AttributeValue) = 1">
                Success: The Action's Attribute child element shall contain only one child element, namely AttributeValue
            </sch:report>
            
            <sch:assert test="*[1]='urn:ihe:iti:2007:RegistryStoredQuery'
                                        or *[1]='urn:ihe:iti:2007:RegisterDocumentSet-b'
                                        or *[1]='urn:ihe:iti:2010:UpdateDocumentSet'
                                        or *[1]='urn:ihe:iti:2018:RestrictedUpdateDocumentSet'">
                            Error: The AttributeValue of the Action's Attribute child element shall have one of the following values, i.e. urn:ihe:iti:2007:RegistryStoredQuery for ADR due to XDS ITI-18 or urn:ihe:iti:2007:RegisterDocumentSet-b for ADR due to XDS ITI-42 or urn:ihe:iti:2010:UpdateDocumentSet for ADR due to XDS ITI-57 or urn:ihe:iti:2018:RestrictedUpdateDocumentSet for ADR due to XDS ITI-92
            </sch:assert>
            <sch:report test="*[1]='urn:ihe:iti:2007:RegistryStoredQuery'
                                       or *[1]='urn:ihe:iti:2007:RegisterDocumentSet-b'
                                       or *[1]='urn:ihe:iti:2010:UpdateDocumentSet'
                                       or *[1]='urn:ihe:iti:2018:RestrictedUpdateDocumentSet'">
                            Success: The AttributeValue of the Action's Attribute child element shall have one of the following values, i.e. urn:ihe:iti:2007:RegistryStoredQuery for ADR due to XDS ITI-18 or urn:ihe:iti:2007:RegisterDocumentSet-b for ADR due to XDS ITI-42 or urn:ihe:iti:2010:UpdateDocumentSet for ADR due to XDS ITI-57 or urn:ihe:iti:2018:RestrictedUpdateDocumentSet for ADR due to XDS ITI-92
            </sch:report>
            
        </sch:rule>
        
        <!-- Rule 7: XACMLAuthzDecisionQuery/Request/Environment -->
        &ch_adr_common_part_5_XACMLAuthzDecisionQuery_Request_Environment;

    </sch:pattern>

</sch:schema>