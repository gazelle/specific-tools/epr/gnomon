<?xml version="1.0" encoding="UTF-8"?>

<sch:schema xmlns:sch="http://purl.oclc.org/dsdl/schematron" queryBinding="xslt2"
            xmlns:sqf="http://www.schematron-quickfix.com/validator/process"
            xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <!-- Namespaces -->
    <sch:ns uri="http://www.w3.org/2001/XMLSchema-instance" prefix="xsi" />
    <sch:ns uri="urn:e-health-suisse:2015:policy-administration" prefix="epr" />
    <sch:ns uri="urn:oasis:names:tc:SAML:2.0:assertion"  prefix="saml" />
    <sch:ns uri="http://www.w3.org/2003/05/soap-envelope"  prefix="soap" />
    <sch:ns uri="urn:oasis:names:tc:xacml:2.0:policy:schema:os" prefix="xacml" />
    <sch:ns uri="urn:oasis:names:tc:xacml:2.0:profile:saml2.0:v2:schema:assertion" prefix="xacml-saml" />
    <sch:ns uri="urn:oasis:names:tc:xacml:2.0:profile:saml2.0:v2:schema:protocol" prefix="xacml-samlp" />
    <sch:ns uri="urn:oasis:names:tc:xacml:2.0:context:schema:os" prefix="xacml-context" />
    <sch:ns uri="urn:oasis:names:tc:SAML:2.0:protocol" prefix="samlp" />
    <sch:ns uri="urn:hl7-org:v3" prefix="hl7" />

    <!-- @author a.moustakas -->

    <!-- Variables declared for further use -->
    <sch:let name="regex_oid" value="string('^(\d+\.)*\d+$')" />
    <sch:let name="regex_urn_oid" value="string('^urn:oid:(\d+\.)*\d+$')" />

    <!-- CH-PPQ-0048, CH-PPQ-0062, CH-PPQ-0065, CH-PPQ-0065  -->
    <sch:pattern>

        <sch:rule context="/">

            <sch:assert test="count(*) = 1 and (count(//epr:EprPolicyRepositoryResponse) = 1 or count(//soap:Fault) = 1 or count(//samlp:Response) = 1)">
                Error : The element soap:Body shall convey one of the possible three (3) child elements, meaning epr:EprPolicyRepositoryResponse, soap:Fault or samlp:Response
            </sch:assert>
            <sch:report test="count(*) = 1 and (count(//epr:EprPolicyRepositoryResponse) = 1 or count(//soap:Fault) = 1 or count(//samlp:Response) = 1)">
                Success : The element soap:Body shall convey one of the possible three (3) child elements, meaning epr:EprPolicyRepositoryResponse, soap:Fault or samlp:Response
            </sch:report>

        </sch:rule>

        <!-- CH-PPQ-0049, CH-PPQ-0050, CH-PPQ-0063, CH-PPQ-0064, CH-PPQ-0080, CH-PPQ-0082, CH-PPQ-0083  -->
        <sch:rule context="//epr:EprPolicyRepositoryResponse">

            <sch:assert test="@status='urn:e-health-suisse:2015:response-status:success' or @status='urn:e-health-suisse:2015:response-status:failure'">
                Error : The element epd:PolicyRepositoryResponse shall include an attribute status with value equal to urn:e-health-suisse:2015:response-status:success or urn:e-health-suisse:2015:response-status:failure
            </sch:assert>
            <sch:report test="@status='urn:e-health-suisse:2015:response-status:success'
				or @status='urn:e-health-suisse:2015:response-status:failure'">
                Success : The element epr:PolicyRepositoryResponse shall include an attribute status with value equal to urn:e-health-suisse:2015:response-status:success or urn:e-health-suisse:2015:response-status:failure
            </sch:report>

        </sch:rule>


        <sch:rule context="/soap:Fault">

            <!-- CH-PPQ-0066, CH-PPQ-0067, CH-PPQ-0084, CH-PPQ-0085 -->
            <sch:assert test="count(*) = 3
                            and count(soap:Code) = 1
                            and count(soap:Reason) = 1
                            and count(soap:Detail) = 1">
                Error : The element soap:Fault shall convey exactly three (3) child elements, meaning soap:Code, soap:Reason and soap:Detail.
            </sch:assert>
            <sch:report test="count(*) = 3
                            and count(soap:Code) = 1
                            and count(soap:Reason) = 1
                            and count(soap:Detail) = 1">
                Success : The element soap:Fault shall convey exactly three (3) child elements, meaning soap:Code, soap:Reason and soap:Detail.
            </sch:report>

            <sch:assert test="soap:Detail != ''">
                Error : The soap:Detail element conveyed inside soap:Fault shall be non-empty
            </sch:assert>
            <sch:report test="soap:Detail != ''">
                Success : The soap:Detail element conveyed inside soap:Fault shall be non-empty
            </sch:report>

        </sch:rule>

        <!-- CH-PPQ-0066, CH-PPQ-0084 -->
        <sch:rule context="/soap:Fault/soap:Code">

            <sch:assert test="count(*) = 1
                            and count(soap:Value) = 1
                            and *[1] != ''">
                Error : The soap:Code element conveyed inside soap:Fault shall have exactly one non-empty child element, namely soap:Value
            </sch:assert>
            <sch:report test="count(*) = 1
                            and count(soap:Value) = 1
                            and *[1] != ''">
                Success : The soap:Code element conveyed inside soap:Fault shall have exactly one non-empty child element, namely soap:Value
            </sch:report>

        </sch:rule>

        <sch:rule context="/soap:Fault/soap:Reason">

            <sch:assert test="count(*) = 1
                            and count(soap:Text) = 1
                            and *[1] != ''">
                Error : The soap:Reason element conveyed inside soap:Fault shall have exactly one non-empty child element, namely soap:Text
            </sch:assert>
            <sch:report test="count(*) = 1
                            and count(soap:Text) = 1
                            and *[1] != ''">
                Success : The soap:Reason element conveyed inside soap:Fault shall have exactly one non-empty child element, namely soap:Text
            </sch:report>

            <sch:assert test="*[1]/@xml:lang != ''">
                Error : The soap:Text element under soap:Reason shall include a non-empty attribute named xml:lang to identify the language of the text message (e.g. en, fr, etc)
            </sch:assert>
            <sch:report test="*[1]/@xml:lang != ''">
                Success : The soap:Text element under soap:Reason shall include a non-empty attribute named xml:lang to identify the language of the text message (e.g. en, fr, etc)
            </sch:report>

        </sch:rule>

        <sch:rule context="//soap:Fault/soap:Detail">

            <sch:assert test="count(*) = 1 and count(epr:UnknownPolicySetId) = 1">
                Error : The soap:Detail element conveyed inside soap:Fault shall have exactly one child element, namely UnknownPolicySetId, that
                follows
                the urn:e-health-suisse:2015:policy-administration namespace
            </sch:assert>
            <sch:report test="count(*) = 1 and count(epr:UnknownPolicySetId) = 1">
                Success : The soap:Detail element conveyed inside soap:Fault shall have exactly one child element, namely UnknownPolicySetId, that
                follows
                the urn:e-health-suisse:2015:policy-administration namespace
            </sch:report>

        </sch:rule>

        <sch:rule context="//samlp:Response">

            <!-- CH-PPQ-0025 -->
            <sch:assert test="saml:Assertion">
                Error : The element Response shall have a child named Assertion
            </sch:assert>
            <sch:report test="saml:Assertion">
                Success : The element Response shall have a child named Assertion
            </sch:report>

            <!-- CH-PPQ-0025 -->
            <sch:assert test="samlp:Status">
                Error : The element Response shall have a child named samlp:Status
            </sch:assert>
            <sch:report test="samlp:Status">
                Success : The element Response shall have a child named samlp:Status
            </sch:report>

        </sch:rule>

        <!-- CH-PPQ-0027, CH-PPQ-0028, CH-PPQ-0029 -->
        <sch:rule context="//samlp:Response/samlp:Status">

            <sch:assert test="samlp:StatusCode">
                Error : The element Status shall have a child element named StatusCode
            </sch:assert>
            <sch:report test="samlp:StatusCode">
                Success : The element Status shall have a child element named StatusCode
            </sch:report>

            <sch:assert test="*[1]/@Value = 'urn:oasis:names:tc:SAML:2.0:status:success'
                        or *[1]/@Value = 'urn:oasis:names:tc:SAML:2.0:status:Requester'
                        or *[1]/@Value = 'urn:oasis:names:tc:SAML:2.0:status:Success'
                        or *[1]/@Value = 'urn:oasis:names:tc:SAML:2.0:status:failure'
                        or *[1]/@Value = 'urn:oasis:names:tc:SAML:2.0:status:Responder'
                        or *[1]/@Value = 'urn:oasis:names:tc:SAML:2.0:status:VersionMismatch'
                        or *[1]/@Value = 'urn:e-health-suisse:2015:error:not-holder-of-patient-policies'">
                Error : The child element StatusCode shall have a Value attribute equal to one of the following possible values as defined in OASIS SAML 2.0 Profile of XACML v2.0, i.e. urn:oasis:names:tc:SAML:2.0:status:success,
                urn:oasis:names:tc:SAML:2.0:status:Requester, urn:oasis:names:tc:SAML:2.0:status:Responder,
                urn:oasis:names:tc:SAML:2.0:status:VersionMismatch or other SAML status codes MAY be used where appropriate when there are no XACMLAuthzDecision Assertions present.
            </sch:assert>
            <sch:report test="*[1]/@Value = 'urn:oasis:names:tc:SAML:2.0:status:success'
                        or *[1]/@Value = 'urn:oasis:names:tc:SAML:2.0:status:Requester'
                        or *[1]/@Value = 'urn:oasis:names:tc:SAML:2.0:status:Success'
                        or *[1]/@Value = 'urn:oasis:names:tc:SAML:2.0:status:failure'
                        or *[1]/@Value = 'urn:oasis:names:tc:SAML:2.0:status:Responder'
                        or *[1]/@Value = 'urn:oasis:names:tc:SAML:2.0:status:VersionMismatch'
                        or *[1]/@Value = 'urn:e-health-suisse:2015:error:not-holder-of-patient-policies'">
                Success : The child element StatusCode shall have a Value attribute equal to one of the following possible values as defined in OASIS SAML 2.0 Profile of XACML v2.0, i.e. urn:oasis:names:tc:SAML:2.0:status:success,
                urn:oasis:names:tc:SAML:2.0:status:Requester, urn:oasis:names:tc:SAML:2.0:status:Responder,
                urn:oasis:names:tc:SAML:2.0:status:VersionMismatch or other SAML status codes MAY be used where appropriate when there are no XACMLAuthzDecision Assertions present.
            </sch:report>

        </sch:rule>

        <sch:rule context="//samlp:Response/saml:Assertion">

            <!-- CH-PPQ-0030 -->
            <sch:assert test="saml:Issuer">
                Error : The element Assertion shall have a child named saml:Issuer
            </sch:assert>
            <sch:report test="saml:Issuer">
                Success : The element Assertion shall have a child named saml:Issuer
            </sch:report>

            <!-- CH-PPQ-0036 -->
            <sch:assert test="saml:Statement">
                Error : The element Assertion shall have a child named saml:Statement
            </sch:assert>
            <sch:report test="saml:Statement">
                Success : The element Assertion shall have a child named saml:Statement
            </sch:report>

        </sch:rule>

        <!-- CH-PPQ-0032 -->
        <sch:rule context="//samlp:Response/saml:Assertion/saml:Issuer">

            <sch:assert test="@NameQualifier='urn:e-health-suisse:community-index'">
                Error : The element Issuer shall have an attribute NameQualifier with value urn:e-health-suisse:community-index
            </sch:assert>
            <sch:report test="@NameQualifier='urn:e-health-suisse:community-index'">
                Success : The element Issuer shall have an attribute NameQualifier with value urn:e-health-suisse:community-index
            </sch:report>

            <sch:assert test="matches(.,$regex_urn_oid)">
                Error : The element Issuer shall contain the home community Id of the Authorization Decision Provider community encoded as an URN
            </sch:assert>
            <sch:report test="matches(.,$regex_urn_oid)">
                Success : The element Issuer shall contain the home community Id of the Authorization Decision Provider community encoded as an URN
            </sch:report>

        </sch:rule>

        <sch:rule context="//samlp:Response[samlp:Status/samlp:StatusCode[@Value='urn:oasis:names:tc:SAML:2.0:status:success']]/saml:Assertion/saml:Statement">

         <!-- CH-PPQ-0037 -->

            <sch:assert test="count(@xsi:type) >= 1">
                Error : The element saml:Statement shall have an attribute xsi:type with xacml-saml:XACMLPolicyStatementType
            </sch:assert>
            <sch:report test="count(@xsi:type) >= 1">
                Success : The element saml:Statement shall have an attribute xsi:type with xacml-saml:XACMLPolicyStatementType
            </sch:report>

           <sch:assert test="count(xacml:Policy) >= 1
                            or count(xacml:PolicySet) >= 1" >
                Error : The element saml:Statement shall have one or more childs named either xacml:Policy or xacml:PolicySet
            </sch:assert>
            <sch:report test="count(xacml:Policy) >= 1
                            or count(xacml:PolicySet) >= 1" >
                Success : The element saml:Statement shall have one or more childs named either xacml:Policy or xacml:PolicySet
            </sch:report>

        </sch:rule>

        <sch:rule context="//samlp:Response[samlp:Status/samlp:StatusCode[@Value='urn:oasis:names:tc:SAML:2.0:status:Success']]/saml:Assertion/saml:Statement">

                 <!-- CH-PPQ-0037 -->

                    <sch:assert test="count(@xsi:type) >= 1">
                        Error : The element saml:Statement shall have an attribute xsi:type with xacml-saml:XACMLPolicyStatementType
                    </sch:assert>
                    <sch:report test="count(@xsi:type) >= 1">
                        Success : The element saml:Statement shall have an attribute xsi:type with xacml-saml:XACMLPolicyStatementType
                    </sch:report>

                   <sch:assert test="count(xacml:Policy) >= 1
                                    or count(xacml:PolicySet) >= 1" >
                        Error : The element saml:Statement shall have one or more childs named either xacml:Policy or xacml:PolicySet
                    </sch:assert>
                    <sch:report test="count(xacml:Policy) >= 1
                                    or count(xacml:PolicySet) >= 1" >
                        Success : The element saml:Statement shall have one or more childs named either xacml:Policy or xacml:PolicySet
                    </sch:report>

         </sch:rule>


        <sch:rule context="//samlp:Response[samlp:Status/samlp:StatusCode[@Value='urn:e-health-suisse:2015:error:not-holder-of-patient-policies']]/saml:Assertion/saml:Statement">

                 <!-- CH-PPQ-0038 -->

                    <sch:assert test="count(@xsi:type) >= 1">
                        Error : The element saml:Statement shall have an attribute xsi:type with xacml-saml:XACMLPolicyStatementType
                    </sch:assert>
                    <sch:report test="count(@xsi:type) >= 1">
                        Success : The element saml:Statement shall have an attribute xsi:type with xacml-saml:XACMLPolicyStatementType
                    </sch:report>

                   <sch:assert test="count(xacml:Policy) >= 0
                                    or count(xacml:PolicySet) >= 0" >
                        Error : The element saml:Statement shall have one or more childs named either xacml:Policy or xacml:PolicySet if policies exist
                    </sch:assert>
                    <sch:report test="count(xacml:Policy) >= 0
                                    or count(xacml:PolicySet) >= 0" >
                        Success : The element saml:Statement shall have one or more childs named either xacml:Policy or xacml:PolicySet if policies exist
                    </sch:report>

                </sch:rule>

    </sch:pattern>
</sch:schema>
