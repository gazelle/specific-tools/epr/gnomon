package com.gnomon.suisse;

import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by kostaskarkaletsis on 04/05/2017.
 */
public class MyErrorHandler implements ErrorHandler {

    private final List<SAXParseException> exceptions = new LinkedList<>();

    public void warning(SAXParseException exception) throws SAXException
    {
        exceptions.add(exception);
    }

    public void fatalError(SAXParseException exception) throws SAXException
    {
        exceptions.add(exception);
    }

    public void error(SAXParseException exception) throws SAXException
    {
        exceptions.add(exception);
    }

    public List<SAXParseException> getExceptions() {
        return exceptions;
    }

}
