package com.gnomon.suisse;

import org.apache.log4j.Logger;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import se.uglisch.XmlSchemaNsUris;

import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.sax.SAXSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.net.URL;


/**
 * Created by kostaskarkaletsis on 02/05/2017.
 */
public class SchematronServices {

    private static Logger logger = Logger.getLogger(SchematronServices.class);

    public static boolean checkValid(String data, String schematron) {
        SchemaFactory sFactory = SchemaFactory.newInstance(XmlSchemaNsUris.SCHEMATRON_NS_URI);
        File inXML = new File(data);
        File inSCH = new File(schematron);
        Source sourceSCH = new StreamSource(inSCH);
        Source sourceXML = new StreamSource(inXML);
        MyErrorHandler errorHandler = new MyErrorHandler();
        try {
            logger.info("Starting validation...");
            Schema myschema = sFactory.newSchema(sourceSCH);
            Validator validator = myschema.newValidator();
            validator.setErrorHandler(errorHandler);
            validator.validate(sourceXML);
        } catch (Exception e) {
            logger.warn(e.getMessage());
            return false;
        } finally {
            logger.info("End of validation");
            logger.info("Found " + errorHandler.getExceptions().size() + " errors");
            for (SAXParseException exception : errorHandler.getExceptions()) {
                logger.info(exception.getMessage() + " (" + exception.getLineNumber() + "," + exception.getColumnNumber() + ")");
            }
        }

        if (errorHandler.getExceptions().isEmpty()) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Check if an XML is valid
     *
     * @param xmlURL                     The XML URL
     * @param xmlContent                 The XML Content
     * @param schematronURL              URL to schematron file location
     * @param schematronContent          The Schematron Content
     * @param urlToISOSchematronMessages
     * @return The schematron validation result
     * @throws TransformerFactoryConfigurationError
     * @throws IOException
     * @throws TransformerException
     */
    public static String checkValid(URL xmlURL,
                                    String xmlContent,
                                    URL schematronURL,
                                    String schematronContent,
                                    URL urlToISOSchematronMessages) throws TransformerFactoryConfigurationError, IOException, TransformerException {
        //First apply the XSL over the schematron content
        InputSource xslSCHMessages = new InputSource();
        if (urlToISOSchematronMessages != null) {
            xslSCHMessages.setSystemId(urlToISOSchematronMessages.toString());
        }
        Transformer newTransformer = TransformerFactory.newInstance().newTransformer(new SAXSource(xslSCHMessages));
        //Apply over the schematron content
        File xslFromSchematron = File.createTempFile("sch", ".xsl");
        Result result = new StreamResult(xslFromSchematron);
        InputSource schematronIS = new InputSource();
        if (schematronURL != null) {
            schematronIS.setSystemId(schematronURL.toString());
        }
        if (schematronContent != null) {
            schematronIS.setCharacterStream(new StringReader(schematronContent));
        }
        newTransformer.transform(new SAXSource(schematronIS), result);

        //Now apply the XSL obtained from applying the Schematron XSL over the SCH file
        newTransformer = TransformerFactory.newInstance().newTransformer(new SAXSource(new InputSource(xslFromSchematron.toURL().toString())));
        StringWriter resultWriter = new StringWriter();
        result = new StreamResult(resultWriter);
        InputSource xmlIS = new InputSource();
        if (xmlURL != null) {
            xmlIS.setSystemId(xmlURL.toString());
        }
        if (xmlContent != null) {
            xmlIS.setCharacterStream(new StringReader(xmlContent));
        }
        newTransformer.transform(new SAXSource(xmlIS), result);

        return resultWriter.toString();
    }

}
