import com.gnomon.suisse.SchematronServices;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by kostaskarkaletsis on 02/05/2017.
 */

public class SchematronServicesTest {

    @Test
    public void ShouldPass() {
        String sample = "src/test/resources/sample1.xml";
        String schematron =  "src/main/resources/schematrons/sample.sch";
        boolean isValid = SchematronServices.checkValid(sample, schematron);
        assertEquals(true,isValid);
    }
    @Test
    public void ShouldReturnErrors() {
        String sample = "src/test/resources/sample2.xml";
        String schematron =  "src/main/resources/schematrons/sample.sch";
        boolean isValid = SchematronServices.checkValid(sample, schematron);
        assertEquals(false,isValid);
    }

}