package com.gnomon.suisse.test;
import com.gnomon.suisse.SchematronServices;
import static org.junit.Assert.assertEquals;
import org.junit.Test;

import org.apache.log4j.Logger;

/**
 *
 * @author a.moustakas
 */
public class PPQ_RequestSchematronServicesTest {
    
    private static final Logger logger = Logger.getLogger(PPQ_RequestSchematronServicesTest.class);
    
    @Test
    public void AddpolicyRequest_step1_emptyAssertion() {
        logger.info("Checking an AddPolicyRequest sample which conveys an empty saml:Assertion (missing saml:Issuer and saml:Statement element)");
        String sample = "src/test/resources/ppq/PPQ_sample_req_AddPolicy_step1.xml";
        String schematron = "src/main/resources/schematrons/PPQ_request_schematron.sch";
        boolean isValid = SchematronServices.checkValid(sample, schematron);
        assertEquals(false, isValid);
    }

    @Test
    public void AddpolicyRequest_step2_withoutIssuer() {
        logger.info("Checking an AddPolicyRequest sample which doesn't have an saml:Issuer element inside the saml:Assertion.");
        String sample = "src/test/resources/ppq/PPQ_sample_req_AddPolicy_step2_without_issuer.xml";
        String schematron = "src/main/resources/schematrons/PPQ_request_schematron.sch";
        boolean isValid = SchematronServices.checkValid(sample, schematron);
        assertEquals(false, isValid);
    }
    
    @Test
    public void AddpolicyRequest_step3_WrongNameQualifier() {
        logger.info("Checking an AddPolicyRequest sample which has a wrong NameQualifier attribute inside the saml:Issuer element.");
        String sample = "src/test/resources/ppq/PPQ_sample_req_AddPolicy_step3_wrong_NameQualifier.xml";
        String schematron = "src/main/resources/schematrons/PPQ_request_schematron.sch";
        boolean isValid = SchematronServices.checkValid(sample, schematron);
        assertEquals(false, isValid);
    }
    
    @Test
    public void AddpolicyRequest_PASS() {
        logger.info("Checking a correct/valid AddPolicyRequest sample.");
        String sample = "src/test/resources/ppq/PPQ_sample_req_AddPolicy_PASS.xml";
        String schematron = "src/main/resources/schematrons/PPQ_request_schematron.sch";
        boolean isValid = SchematronServices.checkValid(sample, schematron);
        assertEquals(true, isValid);
    }
    
    @Test
    public void UpdatePolicyRequest_step1_missingStatement() {
        logger.info("Checking an UpdatePolicyRequest sample which doesn't have a Statement element.");
        String sample = "src/test/resources/ppq/PPQ_sample_req_UpdatePolicy_step1_missingStatement.xml";
        String schematron = "src/main/resources/schematrons/PPQ_request_schematron.sch";
        boolean isValid = SchematronServices.checkValid(sample, schematron);
        assertEquals(false, isValid);
    }
    
    @Test
    public void UpdatePolicyRequest_step2_IssuerWrongURNSyntax() {
        logger.info("Checking an UpdatePolicyRequest sample which contains a saml:Issuer element that does not follow URN syntax");
        String sample = "src/test/resources/ppq/PPQ_sample_req_UpdatePolicy_step2_wrongURNsyntax.xml";
        String schematron = "src/main/resources/schematrons/PPQ_request_schematron.sch";
        boolean isValid = SchematronServices.checkValid(sample, schematron);
        assertEquals(false, isValid);
    }
    
    @Test
    public void UpdatePolicyRequest_PASS() {
        logger.info("Checking a correct UpdatePolicyRequest sample.");
        String sample = "src/test/resources/ppq/PPQ_sample_req_UpdatePolicy_PASS.xml";
        String schematron = "src/main/resources/schematrons/PPQ_request_schematron.sch";
        boolean isValid = SchematronServices.checkValid(sample, schematron);
        assertEquals(true, isValid);
    }
    
    @Test
    public void DeletePolicyRequest_step1_missingPolicySetIdReference() {
        logger.info("Checking an DeletePolicyRequest sample which doesn't have a PolicySetIdReference element.");
        String sample = "src/test/resources/ppq/PPQ_sample_req_DeletePolicy_step1_without_PolicySetIdReference.xml";
        String schematron = "src/main/resources/schematrons/PPQ_request_schematron.sch";
        boolean isValid = SchematronServices.checkValid(sample, schematron);
        assertEquals(false, isValid);
    }
    
    @Test
    public void DeletePolicyRequest_missingIssuerElement() {
        logger.info("Checking an DeletePolicyRequest sample which doesn't have a saml:Issuer element under the saml:Assertion.");
        String sample = "src/test/resources/ppq/PPQ_sample_req_DeletePolicy_step2_missingIssuerElement.xml";
        String schematron = "src/main/resources/schematrons/PPQ_request_schematron.sch";
        boolean isValid = SchematronServices.checkValid(sample, schematron);
        assertEquals(false, isValid);
    }
    
    @Test
    public void DeletePolicyRequest_emptyPolicySetIdReference() {
        logger.info("Checking an DeletePolicyRequest sample with an empty xacml:PolicySetIdReference element under saml:Statement.");
        String sample = "src/test/resources/ppq/PPQ_sample_req_DeletePolicy_step3_emptyPolicySetIdReference.xml";
        String schematron = "src/main/resources/schematrons/PPQ_request_schematron.sch";
        boolean isValid = SchematronServices.checkValid(sample, schematron);
        assertEquals(false, isValid);
    }
    
    @Test
    public void DeletePolicyRequest_PASS() {
        logger.info("Checking a correct DeletePolicyRequest sample.");
        String sample = "src/test/resources/ppq/PPQ_sample_req_DeletePolicy_PASS.xml";
        String schematron = "src/main/resources/schematrons/PPQ_request_schematron.sch";
        boolean isValid = SchematronServices.checkValid(sample, schematron);
        assertEquals(true, isValid);
    }

    @Test
    public void XACMLPolicyRequest_step1() {
        logger.info("Checking an XACMLPolicyRequest sample which contains an empty Request element.");
        String sample = "src/test/resources/ppq/PPQ_sample_req_XACMLPolicy_step1.xml";
        String schematron = "src/main/resources/schematrons/PPQ_request_schematron.sch";
        boolean isValid = SchematronServices.checkValid(sample, schematron);
        assertEquals(false, isValid);
    }

    @Test
    public void XACMLPolicyRequest_step2() {
        logger.info("Checking an XACMLPolicyRequest sample which contains an empty Resource element under the Request.");
        String sample = "src/test/resources/ppq/PPQ_sample_req_XACMLPolicy_step2.xml";
        String schematron = "src/main/resources/schematrons/PPQ_request_schematron.sch";
        boolean isValid = SchematronServices.checkValid(sample, schematron);
        assertEquals(false, isValid);
    }

    @Test
    public void XACMLPolicyRequest_step3_missingAttributes() {
        logger.info("Checking an XACMLPolicyRequest sample which contains an empty Request:Resource:Attribute element with missing attributes inside.");
        String sample = "src/test/resources/ppq/PPQ_sample_req_XACMLPolicy_step3_missingAttributes.xml";
        String schematron = "src/main/resources/schematrons/PPQ_request_schematron.sch";
        boolean isValid = SchematronServices.checkValid(sample, schematron);
        assertEquals(false, isValid);
    }
    
    @Test
    public void XACMLPolicyRequest_step4_missingHL7element() {
        logger.info("Checking an XACMLPolicyRequest sample which contains an empty Request:Resource:Attribute:AttributeValue element.");
        String sample = "src/test/resources/ppq/PPQ_sample_req_XACMLPolicy_step4_missingHL7element.xml";
        String schematron = "src/main/resources/schematrons/PPQ_request_schematron.sch";
        boolean isValid = SchematronServices.checkValid(sample, schematron);
        assertEquals(false, isValid);
    }
    
    @Test
    public void XACMLPolicyRequest_PASS() {
        logger.info("Checking a correct XACMLPolicyRequest sample.");
        String sample = "src/test/resources/ppq/PPQ_sample_req_XACMLPolicy_step5_PASS.xml";
        String schematron = "src/main/resources/schematrons/PPQ_request_schematron.sch";
        boolean isValid = SchematronServices.checkValid(sample, schematron);
        assertEquals(true, isValid);
    }
    
        
}
