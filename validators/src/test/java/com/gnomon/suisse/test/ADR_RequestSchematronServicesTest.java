
package com.gnomon.suisse.test;

import com.gnomon.suisse.SchematronServices;
import org.apache.log4j.Logger;
import static org.junit.Assert.assertEquals;
import org.junit.Test;

/**
 *  Containing test cases for the ADR (due to PPQ/XDS) request transaction
 *  , using valid & intentionally invalid samples  
 * 
 * @author dzandes-gnomon
 */
public class ADR_RequestSchematronServicesTest {
    
    private static final Logger LOG = Logger.getLogger(ADR_RequestSchematronServicesTest.class);
    
    // CH-ADR-007, CH-ADR-010
    @Test
    public void checkADR_Request_XACMLAuthzDecisionQueryBasicStructure() {
        LOG.info("Checking ADR Request's basic structure (i.e. XACMLAuthzDecisionQuery element, CH-ADR-007 & CH-ADR-010)");
        String sample = "src/test/resources/adr/FAIL_adr_request_sample_1_XACMLAuthzDecisionQuery_Structure.xml";
        String schematron =  "src/main/resources/schematrons/ch_adr_due_to_PPQ_request_schematron.sch";
        boolean isValid = SchematronServices.checkValid(sample, schematron);
        assertEquals(false, isValid);
    }
    
    // CH-ADR-008, CH-ADR-009, CH-ADR-036, CH-ADR-037
    @Test
    public void checkADR_Request_XACMLAuthzDecisionQueryOpeningTagAttributes() {
        LOG.info("Checking XACMLAuthzDecisionQuery element's opening tag attributes (i.e CH-ADR-008, CH-ADR-009). "
                + "The absence (or not) of the latter attributes will have an impact to the Request's Environment child element (i.e. CH-ADR-036, CH-ADR-037)");
        String sample = "src/test/resources/adr/FAIL_adr_request_sample_2_XACMLAuthzDecisionQuery_Opening_Tags_Attributes.xml";
        String schematron =  "src/main/resources/schematrons/ch_adr_due_to_PPQ_request_schematron.sch";
        boolean isValid = SchematronServices.checkValid(sample, schematron);
        assertEquals(false, isValid);
    }
    
    // CH-ADR-011, CH-ADR-012, CH-ADR-013, CH-ADR-014
    @Test
    public void checkADR_Request_RequestElementBasicStructure() {
        LOG.info("Checking the basic structure of XACMLAuthzDecisionQuery's child element, namely xacml-context:Request (i.e. CH-ADR-011, CH-ADR-012, CH-ADR-013)");
        String sample = "src/test/resources/adr/FAIL_adr_request_sample_3_Request_Element_Structure.xml";
        String schematron =  "src/main/resources/schematrons/ch_adr_due_to_PPQ_request_schematron.sch";
        boolean isValid = SchematronServices.checkValid(sample, schematron);
        assertEquals(false, isValid);
    }
    
    // CH-ADR-015
    @Test
    public void checkADR_Request_SubjectElementBasicStructure() {
        LOG.info("Checking the basic structure of the Request's first child element named Subject (i.e. CH-ADR-015)");
        String sample = "src/test/resources/adr/FAIL_adr_request_sample_4_Subject_Element_Structure.xml";
        String schematron =  "src/main/resources/schematrons/ch_adr_due_to_PPQ_request_schematron.sch";
        boolean isValid = SchematronServices.checkValid(sample, schematron);
        assertEquals(false, isValid);
    }
    
    // CH-ADR-016
    @Test
    public void checkADR_Request_SubjectElementSubjectidAttributeExistence() {
        LOG.info("Checking the existence of Subject's Attribute child element with AttributeId equal to urn:oasis:names:tc:xacml:1.0:subject:subject-id (i.e. CH-ADR-016)");
        String sample = "src/test/resources/adr/FAIL_adr_request_sample_5_Subject_Element_Missing_Subject_id_Attribute.xml";
        String schematron =  "src/main/resources/schematrons/ch_adr_due_to_PPQ_request_schematron.sch";
        boolean isValid = SchematronServices.checkValid(sample, schematron);
        assertEquals(false, isValid);
    }
    
    // CH-ADR-016
    @Test
    public void checkADR_Request_SubjectElementSubjectidAttributeCorrectness() {
        LOG.info("Checking the correctness of Subject's Attribute child element with AttributeId equal to urn:oasis:names:tc:xacml:1.0:subject:subject-id (i.e. CH-ADR-016)");
        String sample = "src/test/resources/adr/FAIL_adr_request_sample_6_Subject_Element_Wrong_Subject_id_Attribute.xml";
        String schematron =  "src/main/resources/schematrons/ch_adr_due_to_PPQ_request_schematron.sch";
        boolean isValid = SchematronServices.checkValid(sample, schematron);
        assertEquals(false, isValid);
    }
    
    // CH-ADR-018
    @Test
    public void checkADR_Request_SubjectElementSubjectidqualifierAttributeExistence() {
        LOG.info("Checking the existence of Subject's Attribute child element with AttributeId equal to urn:oasis:names:tc:xacml:1.0:subject:subject-id-qualifier (i.e. CH-ADR-018)");
        String sample = "src/test/resources/adr/FAIL_adr_request_sample_7_Subject_Element_Missing_Subject_id_qualifier_Attribute.xml";
        String schematron =  "src/main/resources/schematrons/ch_adr_due_to_PPQ_request_schematron.sch";
        boolean isValid = SchematronServices.checkValid(sample, schematron);
        assertEquals(false, isValid);
    }
    
    // CH-ADR-018
    @Test
    public void checkADR_Request_SubjectElementSubjectidqualifierAttributeCorrectness() {
        LOG.info("Checking the correctness of Subject's Attribute child element with AttributeId equal to urn:oasis:names:tc:xacml:1.0:subject:subject-id-qualifier (i.e. CH-ADR-018)");
        String sample = "src/test/resources/adr/FAIL_adr_request_sample_8_Subject_Element_Wrong_Subject_id_qualifier_Attribute.xml";
        String schematron =  "src/main/resources/schematrons/ch_adr_due_to_PPQ_request_schematron.sch";
        boolean isValid = SchematronServices.checkValid(sample, schematron);
        assertEquals(false, isValid);
    }
    
    // CH-ADR-019
    @Test
    public void checkADR_Request_SubjectElementSubjectidqualifierAttributeWrongValue() {
        LOG.info("Checking the value of Subject's Attribute child element with AttributeId equal to urn:oasis:names:tc:xacml:1.0:subject:subject-id-qualifier (i.e. CH-ADR-019)");
        String sample = "src/test/resources/adr/FAIL_adr_request_sample_27_Subject_Element_InvalidInput_Subjectidqualifier_Attribute.xml";
        String schematron =  "src/main/resources/schematrons/ch_adr_due_to_PPQ_request_schematron.sch";
        boolean isValid = SchematronServices.checkValid(sample, schematron);
        assertEquals(false, isValid);
    }
    
    // CH-ADR-021
    @Test
    public void checkADR_Request_SubjectElementHomeCommunityIdAttributeExistence() {
        LOG.info("Checking the existence of Subject's Attribute child element with AttributeId equal to urn:ihe:iti:xca:2010:homeCommunityId (i.e. CH-ADR-021)");
        String sample = "src/test/resources/adr/FAIL_adr_request_sample_9_Subject_Element_Missing_homeCommunityId_Attribute.xml";
        String schematron =  "src/main/resources/schematrons/ch_adr_due_to_PPQ_request_schematron.sch";
        boolean isValid = SchematronServices.checkValid(sample, schematron);
        assertEquals(false, isValid);
    }
    
    // CH-ADR-021
    @Test
    public void checkADR_Request_SubjectElementHomeCommunityIdAttributeCorrectness() {
        LOG.info("Checking the correctness of Subject's Attribute child element with AttributeId equal to urn:ihe:iti:xca:2010:homeCommunityId (i.e. CH-ADR-021)");
        String sample = "src/test/resources/adr/FAIL_adr_request_sample_10_Subject_Element_Wrong_homeCommunityId_Attribute.xml";
        String schematron =  "src/main/resources/schematrons/ch_adr_due_to_PPQ_request_schematron.sch";
        boolean isValid = SchematronServices.checkValid(sample, schematron);
        assertEquals(false, isValid);
    }
    
    // CH-ADR-022
    @Test
    public void checkADR_Request_SubjectElementHomeCommunityIdAttribute_OIDSyntax() {
        LOG.info("Checking the OID syntax of Subject's Attribute child element with AttributeId equal to urn:ihe:iti:xca:2010:homeCommunityId (i.e. CH-ADR-022)");
        String sample = "src/test/resources/adr/FAIL_adr_request_sample_11_Subject_Element_OID_SyntaxError_homeCommunityId_Attribute.xml";
        String schematron =  "src/main/resources/schematrons/ch_adr_due_to_PPQ_request_schematron.sch";
        boolean isValid = SchematronServices.checkValid(sample, schematron);
        assertEquals(false, isValid);
    }
    
    // CH-ADR-023
    @Test
    public void checkADR_Request_SubjectElementSubjectroleAttributeExistence() {
        LOG.info("Checking the existence of Subject's Attribute child element with AttributeId equal to urn:oasis:names:tc:xacml:2.0:subject:role (i.e. CH-ADR-023)");
        String sample = "src/test/resources/adr/FAIL_adr_request_sample_12_Subject_Element_Missing_Subjectrole_Attribute.xml";
        String schematron =  "src/main/resources/schematrons/ch_adr_due_to_PPQ_request_schematron.sch";
        boolean isValid = SchematronServices.checkValid(sample, schematron);
        assertEquals(false, isValid);
    }
    
    // CH-ADR-023
    @Test
    public void checkADR_Request_SubjectElementSubjectroleAttributeCorrectness() {
        LOG.info("Checking the correctness of Subject's Attribute child element with AttributeId equal to urn:oasis:names:tc:xacml:2.0:subject:role (i.e. CH-ADR-023)");
        String sample = "src/test/resources/adr/FAIL_adr_request_sample_13_Subject_Element_Wrong_Subjectrole_Attribute.xml";
        String schematron =  "src/main/resources/schematrons/ch_adr_due_to_PPQ_request_schematron.sch";
        boolean isValid = SchematronServices.checkValid(sample, schematron);
        assertEquals(false, isValid);
    }
    
    // CH-ADR-025
    @Test
    public void checkADR_Request_SubjectElementOrganizationidAttributeExistence() {
        LOG.info("Checking the existence of Subject's Attribute child element with AttributeId equal to urn:oasis:names:tc:xacml:2.0:subject:organization-id (i.e. CH-ADR-025)");
        String sample = "src/test/resources/adr/FAIL_adr_request_sample_14_Subject_Element_Missing_Organizationid_Attribute.xml";
        String schematron =  "src/main/resources/schematrons/ch_adr_due_to_PPQ_request_schematron.sch";
        boolean isValid = SchematronServices.checkValid(sample, schematron);
        assertEquals(false, isValid);
    }
    
    // CH-ADR-025
    @Test
    public void checkADR_Request_SubjectElementOrganizationidAttributeCorrectness() {
        LOG.info("Checking the correctness of Subject's Attribute child element with AttributeId equal to urn:oasis:names:tc:xacml:2.0:subject:organization-id (i.e. CH-ADR-025)");
        String sample = "src/test/resources/adr/FAIL_adr_request_sample_15_Subject_Element_Wrong_Organizationid_Attribute.xml";
        String schematron =  "src/main/resources/schematrons/ch_adr_due_to_PPQ_request_schematron.sch";
        boolean isValid = SchematronServices.checkValid(sample, schematron);
        assertEquals(false, isValid);
    }
    
    // CH-ADR-026
    @Test
    public void checkADR_Request_SubjectElementOrganizationidAttribute_URNSyntax() {
        LOG.info("Checking the URN Syntax of Subject's Attribute child element with AttributeId equal to urn:oasis:names:tc:xacml:2.0:subject:organization-id (i.e. CH-ADR-026)");
        String sample = "src/test/resources/adr/FAIL_adr_request_sample_16_Subject_Element_URN_SyntaxError_Organizationid_Attribute.xml";
        String schematron =  "src/main/resources/schematrons/ch_adr_due_to_PPQ_request_schematron.sch";
        boolean isValid = SchematronServices.checkValid(sample, schematron);
        assertEquals(false, isValid);
    }
    
    // CH-ADR-028
    @Test
    public void checkADR_Request_SubjectElementPurposeofuseAttributeExistence() {
        LOG.info("Checking the existence of Subject's Attribute child element with AttributeId equal to urn:oasis:names:tc:xacml:2.0:subject:purposeofuse (i.e. CH-ADR-028)");
        String sample = "src/test/resources/adr/FAIL_adr_request_sample_17_Subject_Element_Missing_Purposeofuse_Attribute.xml";
        String schematron =  "src/main/resources/schematrons/ch_adr_due_to_PPQ_request_schematron.sch";
        boolean isValid = SchematronServices.checkValid(sample, schematron);
        assertEquals(false, isValid);
    }
    
    // CH-ADR-028
    @Test
    public void checkADR_Request_SubjectElementPurposeofuseAttributeCorrectness() {
        LOG.info("Checking the existence of Subject's Attribute child element with AttributeId equal to urn:oasis:names:tc:xacml:2.0:subject:purposeofuse (i.e. CH-ADR-028)");
        String sample = "src/test/resources/adr/FAIL_adr_request_sample_18_Subject_Element_Wrong_Purposeofuse_Attribute.xml";
        String schematron =  "src/main/resources/schematrons/ch_adr_due_to_PPQ_request_schematron.sch";
        boolean isValid = SchematronServices.checkValid(sample, schematron);
        assertEquals(false, isValid);
    }
    
    // CH-ADR-030, CH-ADR-031
    @Test
    public void checkADR_Request_ResourceElementBasicStructure() {
        LOG.info("Checking the basic structure of the Request's child element(s) named Resource (i.e. CH-ADR-030, CH-ADR-031)");
        String sample = "src/test/resources/adr/FAIL_adr_request_sample_19_Resource_Element_Structure.xml";
        String schematron =  "src/main/resources/schematrons/ch_adr_due_to_PPQ_request_schematron.sch";
        boolean isValid = SchematronServices.checkValid(sample, schematron);
        assertEquals(false, isValid);
    }
    
    // CH-ADR-031
    @Test
    public void checkADR_Request_ResourceElement_PPQinsteadOfXDS() {
        LOG.info("Checking the case when the Request's child element(s) named Resource refers to \"ADR due to PPQ\" but the schematron selected is the \"ADR due to XDS\"  (i.e. CH-ADR-031)");
        String sample = "src/test/resources/adr/FAIL_adr_request_sample_20_Resource_Element_PPQ_instead_of_XDS.xml";
        String schematron =  "src/main/resources/schematrons/ch_adr_due_to_XDS_request_schematron.sch";
        boolean isValid = SchematronServices.checkValid(sample, schematron);
        assertEquals(false, isValid);
    }
    
    // CH-ADR-031
    @Test
    public void checkADR_Request_ResourceElement_XDSinsteadOfPPQ() {
        LOG.info("Checking the case when the Request's child element(s) named Resource refers to \"ADR due to XDS\" but the schematron selected is the \"ADR due to PPQ\"  (i.e. CH-ADR-031)");
        String sample = "src/test/resources/adr/FAIL_adr_request_sample_21_Resource_Element_XDS_instead_of_PPQ.xml";
        String schematron =  "src/main/resources/schematrons/ch_adr_due_to_PPQ_request_schematron.sch";
        boolean isValid = SchematronServices.checkValid(sample, schematron);
        assertEquals(false, isValid);
    }
    
    // CH-ADR-032
    @Test
    public void checkADR_Request_ResourceElement_WrongXDS_InputAttributes() {
        LOG.info("Checking the AttributeValue elements of the Request's child element(s) named Resource when having the \"ADR due to XDS\" case (i.e. CH-ADR-032)");
        String sample = "src/test/resources/adr/FAIL_adr_request_sample_22_Resource_Element_Wrong_XDS_Input_Attributes.xml";
        String schematron =  "src/main/resources/schematrons/ch_adr_due_to_XDS_request_schematron.sch";
        boolean isValid = SchematronServices.checkValid(sample, schematron);
        assertEquals(false, isValid);
    }
    
    // CH-ADR-032
    @Test
    public void checkADR_Request_ResourceElement_WrongPPQ_InputAttributes() {
        LOG.info("Checking the AttributeValue elements of the Request's child element(s) named Resource when having the \"ADR due to PPQ\" case (i.e. CH-ADR-032)");
        String sample = "src/test/resources/adr/FAIL_adr_request_sample_23_Resource_Element_Wrong_PPQ_Input_Attributes.xml";
        String schematron =  "src/main/resources/schematrons/ch_adr_due_to_PPQ_request_schematron.sch";
        boolean isValid = SchematronServices.checkValid(sample, schematron);
        assertEquals(false, isValid);
    }
    
    // CH-ADR-034
    @Test
    public void checkADR_Request_ResourceElement_XDS_MissingAttribute() {
        LOG.info("Checking the ADR:XDS case when the Request/Resource's Attribute child element with AttributeId equal to urn:ihe:iti:xds-b:2007:confidentiality-code is missing (i.e. CH-ADR-034)");
        String sample = "src/test/resources/adr/FAIL_adr_request_sample_24_Resource_Element_Missing_Attribute.xml";
        String schematron =  "src/main/resources/schematrons/ch_adr_due_to_XDS_request_schematron.sch";
        boolean isValid = SchematronServices.checkValid(sample, schematron);
        assertEquals(false, isValid);
    }
    
    // CH-ADR-035
    @Test
    public void checkADR_Request_ResourceElement_PPQ_MissingAttribute() {
        LOG.info("Checking the ADR:PPQ case when the Request/Resource's Attribute child element with AttributeId equal to urn:e-health-suisse:2015:policy-attributes:referenced-policy-set is missing (i.e. CH-ADR-035)");
        String sample = "src/test/resources/adr/FAIL_adr_request_sample_24_Resource_Element_Missing_Attribute.xml";
        String schematron =  "src/main/resources/schematrons/ch_adr_due_to_PPQ_request_schematron.sch";
        boolean isValid = SchematronServices.checkValid(sample, schematron);
        assertEquals(false, isValid);
    }
    
    // CH-ADR-036
    @Test
    public void checkADR_PPQRequest_ActionElement_WrongActionidAttribute() {
        LOG.info("Checking the correctness of an ADR:PPQ Request/Action's Attribute child element with AttributeId equal to urn:oasis:names:tc:xacml:1.0:action-id (i.e. CH-ADR-036)");
        String sample = "src/test/resources/adr/FAIL_adr_request_sample_25_Action_Element_Wrong_PPQ_Actionid_Attribute.xml";
        String schematron =  "src/main/resources/schematrons/ch_adr_due_to_PPQ_request_schematron.sch";
        boolean isValid = SchematronServices.checkValid(sample, schematron);
        assertEquals(false, isValid);
    }
    
    // CH-ADR-037
    @Test
    public void checkADR_XDSRequest_ActionElement_WrongActionidAttribute() {
        LOG.info("Checking the correctness of an ADR:XDS Request/Action's Attribute child element with AttributeId equal to urn:oasis:names:tc:xacml:1.0:action-id (i.e. CH-ADR-037)");
        String sample = "src/test/resources/adr/FAIL_adr_request_sample_26_Action_Element_Wrong_XDS_Actionid_Attribute.xml";
        String schematron =  "src/main/resources/schematrons/ch_adr_due_to_XDS_request_schematron.sch";
        boolean isValid = SchematronServices.checkValid(sample, schematron);
        assertEquals(false, isValid);
    }
    
    // SUCCESS Cases
    @Test
    public void checkADR_PPQRequest_Valid_Sample() {
        LOG.info("Checking an - expected - valid case for ADR due to PPQ Request");
        String sample = "src/test/resources/adr/PASS_ch_adr_due_to_PPQ_request_body.xml";
        String schematron =  "src/main/resources/schematrons/ch_adr_due_to_PPQ_request_schematron.sch";
        boolean isValid = SchematronServices.checkValid(sample, schematron);
        assertEquals(true, isValid);
    }
    
    @Test
    public void checkADR_XDSRequest_Valid_Sample() {
        LOG.info("Checking an - expected - valid case for ADR due to XDS Request");
        String sample = "src/test/resources/adr/PASS_ch_adr_due_to_XDS_request_body.xml";
        String schematron =  "src/main/resources/schematrons/ch_adr_due_to_XDS_request_schematron.sch";
        boolean isValid = SchematronServices.checkValid(sample, schematron);
        assertEquals(true, isValid);
    }
    
}
