package com.gnomon.suisse.test;
import com.gnomon.suisse.SchematronServices;
import org.apache.log4j.Logger;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 *
 * @author a.moustakas
 */
public class PPQ_ResponseSchematronServicesTest {
    
    private static final Logger LOGGER = Logger.getLogger(PPQ_ResponseSchematronServicesTest.class);
    
    @Test
    public void AddOrUpdateOrDeletePolicyResponse_SuccessSpellingMistake() {
        LOGGER.info("Checking an Add or an Update or a Delete  success PolicyResponse sample with a spelling mistake.");
        String sample = "src/test/resources/ppq/PPQ_sample_resp_AddOrUpdateOrDeletePolicy_success_spelling_mistake.xml";
        String schematron = "src/main/resources/schematrons/PPQ_response_schematron.sch";
        boolean isValid = SchematronServices.checkValid(sample, schematron);
        assertEquals(false, isValid);
    }
    
    @Test
    public void AddOrUpdateOrDeletePolicyResponse_Success() {
        LOGGER.info("Checking an Add or an Update or a Delete success PolicyResponse which is correct.");
        String sample = "src/test/resources/ppq/PPQ_sample_resp_AddOrUpdateOrDeletePolicy_success_PASS.xml";
        String schematron = "src/main/resources/schematrons/PPQ_response_schematron.sch";
        boolean isValid = SchematronServices.checkValid(sample, schematron);
        assertEquals(true, isValid);
    }
    
    @Test
    public void AddOrUpdateOrDeletePolicyResponse_Failure_WrongStatus() {
        LOGGER.info("Checking an Add or an Update or a Delete  failure PolicyResponse sample with wrong status.");
        String sample = "src/test/resources/ppq/PPQ_sample_resp_AddOrUpdateOrDeletePolicy_failure_wrong_status.xml";
        String schematron = "src/main/resources/schematrons/PPQ_response_schematron.sch";
        boolean isValid = SchematronServices.checkValid(sample, schematron);
        assertEquals(false, isValid);
    }
    
    @Test
    public void AddOrUpdateOrDeletePolicyResponse_validFailure() {
        LOGGER.info("Checking an Add or an Update or a Delete failure PolicyResponse which is correct.");
        String sample = "src/test/resources/ppq/PPQ_sample_resp_AddOrUpdateOrDeletePolicy_failure_PASS.xml";
        String schematron = "src/main/resources/schematrons/PPQ_response_schematron.sch";
        boolean isValid = SchematronServices.checkValid(sample, schematron);
        assertEquals(true, isValid);
    }
    

    @Test
    public void UpdateOrDeletePolicyResponse_UnknownPolicy_missingSoapCode() {
        LOGGER.info("Checking an Update or a Delete Unkown PolicyResponse which doesn't have a soap:Code element.");
        String sample = "src/test/resources/ppq/PPQ_sample_resp_UpdateOrDeletePolicy_unknownPolicy_missingSoapCode.xml";
        String schematron = "src/main/resources/schematrons/PPQ_response_schematron.sch";
        boolean isValid = SchematronServices.checkValid(sample, schematron);
        assertEquals(false, isValid);
    }
    
    @Test
    public void UpdateOrDeletePolicyResponse_UnknownPolicy_soapCodeEmptySoapValue() {
        LOGGER.info("Checking an Update or a Delete Unkown PolicyResponse which returns an empty soap:Value under soap:Code element.");
        String sample = "src/test/resources/ppq/PPQ_sample_resp_UpdateOrDeletePolicy_unknownPolicy_soapCodeEmptySoapValue.xml";
        String schematron = "src/main/resources/schematrons/PPQ_response_schematron.sch";
        boolean isValid = SchematronServices.checkValid(sample, schematron);
        assertEquals(false, isValid);
    }
    
    @Test
    public void UpdateOrDeletePolicyResponse_UnknownPolicy_soapReasonEmptySoapText() {
        LOGGER.info("Checking an Update or a Delete Unkown PolicyResponse which returns an empty soap:Text under soap:Reason element.");
        String sample = "src/test/resources/ppq/PPQ_sample_resp_UpdateOrDeletePolicy_unknownPolicy_soapReasonEmptySoapText.xml";
        String schematron = "src/main/resources/schematrons/PPQ_response_schematron.sch";
        boolean isValid = SchematronServices.checkValid(sample, schematron);
        assertEquals(false, isValid);
    }
    
    @Test
    public void UpdateOrDeletePolicyResponse_UnknownPolicy_soapTextMissingAttribute() {
        LOGGER.info("Checking an Update or a Delete Unkown PolicyResponse which returns a soap:Text with missing xml:lang attribute under soap:Reason element.");
        String sample = "src/test/resources/ppq/PPQ_sample_resp_UpdateOrDeletePolicy_unknownPolicy_soapTextMissingLangAttribute.xml";
        String schematron = "src/main/resources/schematrons/PPQ_response_schematron.sch";
        boolean isValid = SchematronServices.checkValid(sample, schematron);
        assertEquals(false, isValid);
    }
    
    @Test
    public void UpdateOrDeletePolicyResponse_UnknownPolicy_emptySoapDetail() {
        LOGGER.info("Checking an Update or a Delete Unkown PolicyResponse which returns an empty soap:Detail under soap:Fault element");
        String sample = "src/test/resources/ppq/PPQ_sample_resp_UpdateOrDeletePolicy_unknownPolicy_emptySoapDetail.xml";
        String schematron = "src/main/resources/schematrons/PPQ_response_schematron.sch";
        boolean isValid = SchematronServices.checkValid(sample, schematron);
        assertEquals(false, isValid);
    }
    
    @Test
    public void UpdateOrDeletePolicyResponse_UnknownPolicy_PASS() {
        LOGGER.info("Checking an Update or a Delete Unkown Policy Response which is correct/valid.");
        String sample = "src/test/resources/ppq/PPQ_sample_resp_UpdateOrDeletePolicy_unknown_policy_PASS.xml";
        String schematron = "src/main/resources/schematrons/PPQ_response_schematron.sch";
        boolean isValid = SchematronServices.checkValid(sample, schematron);
        assertEquals(true, isValid);
    }
    
    
    @Test
    public void XACMLPolicyResponse_step1() {
        LOGGER.info("Checking an XACML PolicyResponse which doesn't have a Status, a StatusCode, a Statement and a Policy element.");
        String sample = "src/test/resources/ppq/PPQ_sample_resp_XACMLPolicy_step1.xml";
        String schematron = "src/main/resources/schematrons/PPQ_response_schematron.sch";
        boolean isValid = SchematronServices.checkValid(sample, schematron);
        assertEquals(false, isValid);
    }
    
    @Test
    public void XACMLPolicyResponse_step2_withoutStatusCode() {
        LOGGER.info("Checking an XACML PolicyResponse which doesn't have a StatusCode element.");
        String sample = "src/test/resources/ppq/PPQ_sample_resp_XACMLPolicy_step2_without_StatusCode.xml";
        String schematron = "src/main/resources/schematrons/PPQ_response_schematron.sch";
        boolean isValid = SchematronServices.checkValid(sample, schematron);
        assertEquals(false, isValid);
    }
    
    @Test
    public void XACMLPolicyResponse_step3_withoutStatementElement() {
        LOGGER.info("Checking an XACML PolicyResponse which doesn't have a Statement element.");
        String sample ="src/test/resources/ppq/PPQ_sample_resp_XACMLPolicy_step3_without_StatementElement.xml";
        String schematron = "src/main/resources/schematrons/PPQ_response_schematron.sch";
        boolean isValid = SchematronServices.checkValid(sample, schematron);
        assertEquals(false, isValid);
    }
    
   @Test
    public void XACMLPolicyResponse_step4_withoutPolicies() {
        LOGGER.info("Checking an XACML PolicyResponse which doesn't have a Policy element.");
        String sample = "src/test/resources/ppq/PPQ_sample_resp_XACMLPolicy_step4_without_policies.xml";
        String schematron = "src/main/resources/schematrons/PPQ_response_schematron.sch";
        boolean isValid = SchematronServices.checkValid(sample, schematron);
        assertEquals(false, isValid);
    }
    
    @Test
    public void XACMLPolicyResponse_step5_wrongIssuerURN() {
        LOGGER.info("Checking an XACML PolicyResponse which have a wrong Issuer urn.");
        String sample = "src/test/resources/ppq/PPQ_sample_resp_XACMLPolicy_step5_wrong_issuer_urn.xml";
        String schematron = "src/main/resources/schematrons/PPQ_response_schematron.sch";
        boolean isValid = SchematronServices.checkValid(sample, schematron);
        assertEquals(false, isValid);
    }
    
    @Test
    public void XACMLPolicyResponse_PASS() {
        LOGGER.info("Checking a correct XACML PolicyResponse.");
        String sample = "src/test/resources/ppq/PPQ_sample_resp_XACMLPolicy_PASS.xml";
        String schematron = "src/main/resources/schematrons/PPQ_response_schematron.sch";
        boolean isValid = SchematronServices.checkValid(sample, schematron);
        assertEquals(true, isValid);
    }

    @Test
    public void XACMLPolicyResponseDifferentNameSpaceForHL7_PASS() {
        LOGGER.info("Checking a correct XACML PolicyResponse.");
        String sample = "src/test/resources/ppq/ch_ppq_policy_query_by_patient_and_policy_set_no_soap.xml";
        String schematron = "src/main/resources/schematrons/PPQ_request_schematron.sch";
        boolean isValid = SchematronServices.checkValid(sample, schematron);
        assertEquals(true, isValid);
    }
}