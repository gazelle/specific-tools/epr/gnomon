/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gnomon.suisse.test;

import com.gnomon.suisse.SchematronServices;
import org.apache.log4j.Logger;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Containing test cases for the ADR response transaction
 * , using valid & intentionally invalid samples
 *
 * @author dzandes
 */
public class ADR_ResponseSchematronServicesTest {

    private static final Logger LOG = Logger.getLogger(ADR_ResponseSchematronServicesTest.class);

    // CH-ADR-044
    @Test
    public void checkADR_Response_Wrong_soapHeader() {
        LOG.info("Checking ADR's Response SOAP Header (i.e. CH-ADR-044)");
        String sample = "src/test/resources/adr/response/FAIL_adr_response_sample_1_Wrong_SoapHeader.xml";
        String schematron = "src/main/resources/schematrons/ch_adr_due_to_PPQ_response_schematron.sch";
        boolean isValid = SchematronServices.checkValid(sample, schematron);
        assertEquals(false, isValid);
    }

    // CH-ADR-045
    @Test
    public void checkADR_Response_Missing_bodyAssertion() {
        LOG.info("Checking ADR's Response SOAP Body when Assertion is missing (i.e. CH-ADR-045)");
        String sample = "src/test/resources/adr/response/FAIL_adr_response_sample_2_Missing_BodyAssertion.xml";
        String schematron = "src/main/resources/schematrons/ch_adr_due_to_PPQ_response_schematron.sch";
        boolean isValid = SchematronServices.checkValid(sample, schematron);
        assertEquals(false, isValid);
    }

    // CH-ADR-046
    @Test
    public void checkADR_Response_Missing_authzStatement() {
        LOG.info(
                "Checking ADR's Response SOAP Body when the XACML Authorization Statement (i.e. saml:Statement) inside the Assertion is missing (i" +
                        ".e. CH-ADR-046)");
        String sample = "src/test/resources/adr/response/FAIL_adr_response_sample_3_Missing_AuthzStatement.xml";
        String schematron = "src/main/resources/schematrons/ch_adr_due_to_PPQ_response_schematron.sch";
        boolean isValid = SchematronServices.checkValid(sample, schematron);
        assertEquals(false, isValid);
    }

    // CH-ADR-049
    @Test
    public void checkADR_Response_Missing_IssuerElement() {
        LOG.info("Checking ADR's Response SOAP Body when the Issuer (i.e. saml:Issuer) inside the Assertion is missing (i.e. CH-ADR-049)");
        String sample = "src/test/resources/adr/response/FAIL_adr_response_sample_4_Missing_Issuer.xml";
        String schematron = "src/main/resources/schematrons/ch_adr_due_to_PPQ_response_schematron.sch";
        boolean isValid = SchematronServices.checkValid(sample, schematron);
        assertEquals(false, isValid);
    }

    // CH-ADR-049
    @Test
    public void checkADR_Response_Empty_IssuerElement() {
        LOG.info("Checking ADR's Response SOAP Body when the Issuer (i.e. saml:Issuer) inside the Assertion is empty (i.e. CH-ADR-049)");
        String sample = "src/test/resources/adr/response/FAIL_adr_response_sample_5_Empty_Issuer.xml";
        String schematron = "src/main/resources/schematrons/ch_adr_due_to_PPQ_response_schematron.sch";
        boolean isValid = SchematronServices.checkValid(sample, schematron);
        assertEquals(false, isValid);
    }

    // CH-ADR-049
    @Test
    public void checkADR_Response_WrongSyntax_IssuerElement() {
        LOG.info("Checking ADR's Response SOAP Body when the Issuer (i.e. saml:Issuer) inside the Assertion has wrong values (i.e. CH-ADR-049)");
        String sample = "src/test/resources/adr/response/FAIL_adr_response_sample_6_WrongSyntax_Issuer.xml";
        String schematron = "src/main/resources/schematrons/ch_adr_due_to_PPQ_response_schematron.sch";
        boolean isValid = SchematronServices.checkValid(sample, schematron);
        assertEquals(false, isValid);
    }

    // CH-ADR-050
    @Test
    public void checkADR_Response_EmptyResponse_Element() {
        LOG.info("Checking ADR's Response SOAP Body when the Response element under Assertion/Statement is empty (i.e. CH-ADR-050)");
        String sample = "src/test/resources/adr/response/FAIL_adr_response_sample_7_EmptyResponseElement.xml";
        String schematron = "src/main/resources/schematrons/ch_adr_due_to_PPQ_response_schematron.sch";
        boolean isValid = SchematronServices.checkValid(sample, schematron);
        assertEquals(false, isValid);
    }

    // CH-ADR-051
    @Test
    public void checkADR_Response_MissingResourceId_Attribute() {
        LOG.info("Checking ADR's Response when the ResourceId attribute under Assertion/Statement/Response/Result is missing (i.e. CH-ADR-051)");
        String sample = "src/test/resources/adr/response/FAIL_adr_response_sample_8_Missing_ResourceId_Attribute.xml";
        String schematron = "src/main/resources/schematrons/ch_adr_due_to_PPQ_response_schematron.sch";
        boolean isValid = SchematronServices.checkValid(sample, schematron);
        assertEquals(false, isValid);
    }

    // CH-ADR-051
    @Test
    public void checkADR_Response_EmptyResourceId_Attribute() {
        LOG.info("Checking ADR's Response when the ResourceId attribute under Assertion/Statement/Response/Result is empty (i.e. CH-ADR-051)");
        String sample = "src/test/resources/adr/response/FAIL_adr_response_sample_9_Empty_ResourceId_Attribute.xml";
        String schematron = "src/main/resources/schematrons/ch_adr_due_to_PPQ_response_schematron.sch";
        boolean isValid = SchematronServices.checkValid(sample, schematron);
        assertEquals(false, isValid);
    }

    // CH-ADR-051 (for "due to XDS" case)
    @Test
    public void checkADRdueToXDS_Response_ResourceId_SyntaxError() {
        LOG.info(
                "Checking ADR's due to XDS Response when the ResourceId attribute under Assertion/Statement/Response/Result does not comply with " +
                        "the syntax rules (i.e. CH-ADR-051)");
        String sample = "src/test/resources/adr/response/FAIL_adr_dueToXDS_response_sample_18_SyntaxErrorResourceid.xml";
        String schematron = "src/main/resources/schematrons/ch_adr_due_to_XDS_response_schematron.sch";
        boolean isValid = SchematronServices.checkValid(sample, schematron);
        assertEquals(false, isValid);
    }

    // CH-ADR-051 (for "due to XDS" case)
    @Test
    public void checkADRdueToXDS_Response_ResourceId_SyntaxError_II() {
        LOG.info(
                "Checking ADR's due to XDS Response when the ResourceId attribute under Assertion/Statement/Response/Result does not comply with " +
                        "the syntax rules (i.e. CH-ADR-051)");
        String sample = "src/test/resources/adr/response/FAIL_adr_dueToXDS_response_sample_19_SyntaxErrorResourceid_II.xml";
        String schematron = "src/main/resources/schematrons/ch_adr_due_to_XDS_response_schematron.sch";
        boolean isValid = SchematronServices.checkValid(sample, schematron);
        assertEquals(false, isValid);
    }

    // CH-ADR-052
    @Test
    public void checkADR_Response_WrongDecisionValue_ResultElement() {
        LOG.info("Checking ADR's Response when the Decision value under Assertion/Statement/Response/Result/Decision is wrong (i.e. CH-ADR-052)");
        String sample = "src/test/resources/adr/response/FAIL_adr_response_sample_10_WrongDecisionValue_ResultElement.xml";
        String schematron = "src/main/resources/schematrons/ch_adr_due_to_PPQ_response_schematron.sch";
        boolean isValid = SchematronServices.checkValid(sample, schematron);
        assertEquals(false, isValid);
    }

    // CH-ADR-052
    @Test
    public void checkADR_Response_EmptyDecisionValue_ResultElement() {
        LOG.info("Checking ADR's Response when the Decision value under Assertion/Statement/Response/Result/Decision is empty (i.e. CH-ADR-052)");
        String sample = "src/test/resources/adr/response/FAIL_adr_response_sample_11_EmptyDecisionValue_ResultElement.xml";
        String schematron = "src/main/resources/schematrons/ch_adr_due_to_PPQ_response_schematron.sch";
        boolean isValid = SchematronServices.checkValid(sample, schematron);
        assertEquals(false, isValid);
    }

    // CH-ADR-053
    @Test
    public void checkADR_Response_MissingStatusElement() {
        LOG.info("Checking ADR's Response when the Status element under Assertion/Statement/Response/Result is missing (i.e. CH-ADR-053)");
        String sample = "src/test/resources/adr/response/FAIL_adr_response_sample_12_Missing_StatusElement.xml";
        String schematron = "src/main/resources/schematrons/ch_adr_due_to_PPQ_response_schematron.sch";
        boolean isValid = SchematronServices.checkValid(sample, schematron);
        assertEquals(false, isValid);
    }

    // CH-ADR-053
    @Test
    public void checkADR_Response_EmptyStatusElement() {
        LOG.info("Checking ADR's Response when the Status element under Assertion/Statement/Response/Result is empty (i.e. CH-ADR-053)");
        String sample = "src/test/resources/adr/response/FAIL_adr_response_sample_13_Empty_StatusElement.xml";
        String schematron = "src/main/resources/schematrons/ch_adr_due_to_PPQ_response_schematron.sch";
        boolean isValid = SchematronServices.checkValid(sample, schematron);
        assertEquals(false, isValid);
    }

    // CH-ADR-054
    @Test
    public void checkADR_Response_Wrong_DecisionValueStatusCode_Combination() {
        LOG.info("Checking the case when the combination between Assertion/Statement/Response/Result/Decision and "
                + "Assertion/Statement/Response/Result/Status/StatusCode/@value is wrong (i.e. CH-ADR-054)");
        String sample = "src/test/resources/adr/response/FAIL_adr_response_sample_14_Wrong_DecisionStatusCode_Combo.xml";
        String schematron = "src/main/resources/schematrons/ch_adr_due_to_PPQ_response_schematron.sch";
        boolean isValid = SchematronServices.checkValid(sample, schematron);
        assertEquals(false, isValid);
    }

    // CH-ADR-055
    @Test
    public void checkADR_Response_Wrong_DecisionValueStatusCode_Combination_II() {
        LOG.info("Checking the case when the combination between Assertion/Statement/Response/Result/Decision and "
                + "Assertion/Statement/Response/Result/Status/StatusCode/@value is wrong (i.e. CH-ADR-055)");
        String sample = "src/test/resources/adr/response/FAIL_adr_response_sample_15_Wrong_DecisionStatusCode_Combo_II.xml";
        String schematron = "src/main/resources/schematrons/ch_adr_due_to_PPQ_response_schematron.sch";
        boolean isValid = SchematronServices.checkValid(sample, schematron);
        assertEquals(false, isValid);
    }

    // CH-ADR-048
    @Test
    public void checkADR_Response_Wrong_ResponseStatus() {
        LOG.info(
                "Checking the case when the Response Status under Assertion/Status - taking also into account the Result/Status values - is wrong " +
                        "(i.e. CH-ADR-048)");
        String sample = "src/test/resources/adr/response/FAIL_adr_response_sample_16_Wrong_ResponseStatus.xml";
        String schematron = "src/main/resources/schematrons/ch_adr_due_to_PPQ_response_schematron.sch";
        boolean isValid = SchematronServices.checkValid(sample, schematron);
        assertEquals(false, isValid);
    }

    // CH-ADR-048
    @Test
    public void checkADR_Response_Wrong_ResponseStatus_II() {
        LOG.info(
                "Checking the case when the Response Status under Assertion/Status - taking also into account the Result/Status values - is wrong " +
                        "(i.e. CH-ADR-048)");
        String sample = "src/test/resources/adr/response/FAIL_adr_response_sample_17_Wrong_ResponseStatus_II.xml";
        String schematron = "src/main/resources/schematrons/ch_adr_due_to_PPQ_response_schematron.sch";
        boolean isValid = SchematronServices.checkValid(sample, schematron);
        assertEquals(false, isValid);
    }

    // SUCCESS CASES
    @Test
    public void checkADRdueToPPQ_Valid_Response() {
        LOG.info("Checking a valid sample of an ADR due to PPQ Response message");
        String sample = "src/test/resources/adr/response/ch_adr_due_to_PPQ_valid_response.xml";
        String schematron = "src/main/resources/schematrons/ch_adr_due_to_PPQ_response_schematron.sch";
        boolean isValid = SchematronServices.checkValid(sample, schematron);
        assertEquals(true, isValid);
    }

    @Test
    public void checkADRdueToPPQ_WSDL_based_Valid_Response() {
        String sample = "src/test/resources/adr/response/ch_adr_due_to_PPQ_WSDL_based_valid_response.xml";
        String schematron = "src/main/resources/schematrons/ch_adr_due_to_PPQ_response_schematron.sch";
        boolean isValid = SchematronServices.checkValid(sample, schematron);
        assertEquals(true, isValid);
    }

    @Test
    public void checkADRdueToXDS_Valid_Response() {
        LOG.info("Checking a valid sample of an ADR due to XDS Response message");
        String sample = "src/test/resources/adr/response/ch_adr_due_to_XDS_valid_response.xml";
        String schematron = "src/main/resources/schematrons/ch_adr_due_to_XDS_response_schematron.sch";
        boolean isValid = SchematronServices.checkValid(sample, schematron);
        assertEquals(true, isValid);
    }

    @Test
    public void checkADRdueToXDS_NoSoap_Valid_Response() {
        LOG.info("Checking a valid sample of an ADR due to XDS Response message");
        String sample = "src/test/resources/adr/response/ch_adr_due_to_XDS_valid_response_no_soap.xml";
        String schematron = "src/main/resources/schematrons/ch_adr_due_to_XDS_response_schematron.sch";
        boolean isValid = SchematronServices.checkValid(sample, schematron);
        assertEquals(true, isValid);
    }

    @Test
    public void checkADRdueToXDS_WSDL_based_Valid_Response() {
        String sample = "src/test/resources/adr/response/ch_adr_due_to_XDS_WSDL_based_valid_response.xml";
        String schematron = "src/main/resources/schematrons/ch_adr_due_to_XDS_response_schematron.sch";
        boolean isValid = SchematronServices.checkValid(sample, schematron);
        assertEquals(true, isValid);
    }

}